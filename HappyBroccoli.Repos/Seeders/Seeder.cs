﻿using System;
using System.Collections.Generic;
using HappyBroccoli.DomainEntities.Entities;
using System.Linq;
using System.IO;

namespace HappyBroccoli.Repos.Seeders
{
    public class Seeder
    {
        private RecipesDbContext _context;
        public Seeder(RecipesDbContext context)
        {
            _context = context;
        }
        //supa crema conopida, sfecla rosie, trufe, ulei cocos, salata tabouleh, lipie cu hummus and stuff, sarmale de post, "ciorba de burta", cozonac, pesto, tahini
        public byte[] ReadImage(string path)
        {
            FileStream st = new FileStream(path, FileMode.Open);
            byte[] buffer = new byte[st.Length];
            st.Read(buffer, 0, (int)st.Length);
            st.Close();
            return buffer;
        }

        public void Initialize()
        {
            IList<NutrientCategory> nutrientCategories = new List<NutrientCategory>();
            if (!_context.NutrientCategories.Any())
            {
                IEnumerable<string> categories = new List<string> { "Vitamine", "Minerale", "Macronutrienți" };
                foreach (var category in categories)
                {
                    nutrientCategories.Add(new NutrientCategory { Name = category });
                }

                foreach (NutrientCategory nutrientCategory in nutrientCategories)
                {
                    _context.NutrientCategories.Add(nutrientCategory);
                }
                _context.SaveChanges();
            }
            IList<Ingredient> ingredients = new List<Ingredient>();
            if (!_context.Ingredients.Any())
            {
                IEnumerable<string> names = new List<string> {"sos de tomate", "sare", "piper", "roșii gătite", "tofu", "pătrunjel", "cartofi",
                    "ceapă gătită", "morcovi gătiți", "ardei gras verde gătit", "ardei gras roșu gătit", "gogoșari gătiți", "dovlecei", "păstârnac gătit", "usturoi",
                    "ulei de măsline", "paste", "soia texturat", "oregano uscat", "cimbru", "seminţe de susan", "năut", "coriandru", "făină de grâu", "orez", "chimion", "bicarbonat de sodiu", "boia dulce",
                    "sparanghel", "sos de soia",  "ceapă verde", "ulei de susan", "ghimbir", "fulgi de ardei iute", "silken tofu", "zahăr brun", "curmale", "ciocolată amăruie > 70-85% cacao", "linte verde",
                    "vinete gătite", "conopidă gătită", "frișcă vegetală", "caju", "lapte de soia", "seminţe de chia", "banane", "unt de arahide", "sfeclă gătită", "ulei de cocos", "varză de bruxelles gătită",
                    "mărar", "varză murată", "fasole neagră", "spanac", "spanac gătit", "broccoli", "broccoli gătit", "rucola", "conopidă", "morcovi", "bulgur", "ulei de floarea soarelui", "hummus",
                    "avocado", "zeamă de lămâie", "măsline negre", "tahini", "seminţe de floarea soarelui", "mazăre boabe gătită", "castraveți", "roșii", "quinoa albă gătită"
                };

                foreach (var name in names)
                {
                    ingredients.Add(new Ingredient { Name = name });
                }

                foreach (Ingredient ingredient in ingredients)
                {
                    _context.Ingredients.Add(ingredient);
                }
                _context.SaveChanges();
            }

            IList<Measure> measures = new List<Measure>();
            if (!_context.Measures.Any())
            {
                IEnumerable<string> names = new List<string> { "g", "mg", "kcal", "mcg", "kg", "lg", "lgt", "cană", "căni", "bucăți", "căței", "legătură", "pahar" };

                foreach (var name in names)
                {
                    measures.Add(new Measure { Name = name });
                }

                foreach (Measure measure in measures)
                {
                    _context.Measures.Add(measure);
                }
                _context.SaveChanges();
            }

            IList<Nutrient> nutrients = new List<Nutrient>();


            if (!_context.Nutrients.Any())
            {
                object[,] vitamins = new object[,] { {"A - RAE", 900 }, {"B1 - Tiamină", 1.2}, {"B2 - Riboflavină", 1.3}, {"B3 - Niacină", 16}, {"B4 - Colină", 550}, {"B5 - Acid pantotenic", 5},
                                                { "B6", 1.7}, {"B9 - Acid folic", 400}, {"C", 90}, {"D", 20}, {"E Alfa-tocoferol", 15}, {"K - Filochinonă", 120} };
                object[,] minerals = new object[,] { { "Calciu", 1300 }, { "Fier", 18 }, { "Fosfor", 1250 }, { "Magneziu", 420 }, { "Sodiu", 2400 }, { "Potasiu", 4700 }, { "Zinc", 11 }, { "Cupru", 0.9 }, { "Fluor", 3.5 }, { "Mangan", 2.3 }, { "Seleniu", 55 } };

                object[,] macronutrients = new object[,] { { "Aport caloric", 2000}, { "Apă", 0}, { "Proteine", 50}, { "Fibre", 25}, { "Carbohidrați", 300}, { "Lipide", 65}, { "Glucide", 0}, {"Amidon", 0} };

                var contextNutrientCategories = _context.NutrientCategories.ToList();

                for (int i=0; i < vitamins.GetLength(0); i++)
                {
                    nutrients.Add(new Nutrient { Name = Convert.ToString(vitamins[i,0]), DailyRecommendedValue=Convert.ToDouble(vitamins[i, 1]) , Category = contextNutrientCategories.FirstOrDefault(x => x.Name == "Vitamine") });
                }
                for (int i = 0; i < minerals.GetLength(0); i++)
                {
                    nutrients.Add(new Nutrient { Name = Convert.ToString(minerals[i, 0]), DailyRecommendedValue = Convert.ToDouble(minerals[i, 1]), Category = contextNutrientCategories.FirstOrDefault(x => x.Name == "Minerale") });
                }
                for (int i = 0; i < macronutrients.GetLength(0); i++)
                {
                    nutrients.Add(new Nutrient { Name = Convert.ToString(macronutrients[i, 0]), DailyRecommendedValue = Convert.ToDouble(macronutrients[i, 1]), Category = contextNutrientCategories.FirstOrDefault(x => x.Name == "Macronutrienți") });
                }

                foreach (Nutrient nutrient in nutrients)
                {
                    _context.Nutrients.Add(nutrient);
                }
                _context.SaveChanges();
            }

            IList<NutrientIngredient> nutrientIngredients = new List<NutrientIngredient>();
            if (!_context.NutrientIngredients.Any())
            {
                var contextIngredients = _context.Ingredients.ToList();
                var contextNutrients = _context.Nutrients.ToList();
                var contextMeasures = _context.Measures.ToList();
                object[,] cookedTomatoNutrients = new object[,] { { 80.63, "g", "Apă" }, { 79, "kcal", "Aport caloric" }, { 13.05, "g", "Carbohidrați"}, { 1.7, "g", "Fibre"}, {2.49, "g", "Glucide"},
                                                                  { 2.68, "g", "Lipide" }, {1.96, "g", "Proteine"}, {33, "mcg", "A - RAE" }, {0.108, "mg", "B1 - Tiamină"},
                                                                  {0.08, "mg", "B2 - Riboflavină" }, {1.11, "mg", "B3 - Niacină"}, {0.256, "mg", "B5 - Acid pantotenic" }, {0.086, "mg", "B6"},
                                                                  {18.2, "mg", "C"}, {11, "mcg", "B9 - Acid folic"}, {26, "mg", "Calciu" }, {0.095, "mg", "Cupru"}, {1.06, "mg", "Fier" },
                                                                  {38, "mg", "Fosfor" }, {15, "mg", "Magneziu"}, {0.193, "mg", "Mangan"}, {247, "mg", "Potasiu"}, {1.2, "mcg", "Seleniu"},
                                                                  {455, "mg", "Sodiu"}, {0.18, "mg", "Zinc"} };

                for (int i = 0; i < cookedTomatoNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "roșii gătite"),
                        Quantity = Convert.ToDouble(cookedTomatoNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(cookedTomatoNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(cookedTomatoNutrients[i, 1]))
                    });
                }

                object[,] tomatoNutrients = new object[,] { { 93, "g", "Apă" }, { 23, "kcal", "Aport caloric" }, { 5.1, "g", "Carbohidrați"}, { 1.1, "g", "Fibre"}, {4, "g", "Glucide"},
                    {0.2, "g", "Lipide" }, {1.2, "g", "Proteine"}, {32, "mcg", "A - RAE" }, {0.06, "mg", "B1 - Tiamină"},
                    {0.04, "mg", "B2 - Riboflavină" }, {0.5, "mg", "B3 - Niacină"}, {0.5, "mg", "B5 - Acid pantotenic" }, {0.081, "mg", "B6"},
                    {23.4, "mg", "C"}, {9, "mcg", "B9 - Acid folic"}, {13, "mg", "Calciu" }, {0.09, "mg", "Cupru"}, {0.51, "mg", "Fier" },
                    {28, "mg", "Fosfor" }, {10, "mg", "Magneziu"}, {0.1, "mg", "Mangan"}, {204, "mg", "Potasiu"}, {0.4, "mcg", "Seleniu"},
                    {13, "mg", "Sodiu"}, {0.07, "mg", "Zinc"} };

                for (int i = 0; i < tomatoNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "roșii"),
                        Quantity = Convert.ToDouble(tomatoNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(tomatoNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(tomatoNutrients[i, 1]))
                    });
                }

                object[,] boiledPotatoNutrients = new object[,] { { 77.46, "g", "Apă" }, { 86, "kcal", "Aport caloric" }, { 20, "g", "Carbohidrați"}, { 2, "g", "Fibre"}, {0.85, "g", "Glucide"},
                                                                 { 0.85, "g", "Lipide" }, {1.71, "g", "Proteine"}, {0.098, "mg", "B1 - Tiamină"},
                                                                 {0.019, "mg", "B2 - Riboflavină" }, {1.312, "mg", "B3 - Niacină"}, {0.509, "mg", "B5 - Acid pantotenic" },
                                                                 {0.269, "mg", "B6"}, {7.4, "mg", "C"}, {9, "mcg", "B9 - Acid folic"}, {8, "mg", "Calciu" }, {0.167, "mg", "Cupru"},
                                                                 {0.31, "mg", "Fier" }, {40, "mg", "Fosfor" }, {20, "mg", "Magneziu"}, {0.14, "mg", "Mangan"}, {328, "mg", "Potasiu"},
                                                                 {0.3, "mcg", "Seleniu"}, {241, "mg", "Sodiu"}, {0.27, "mg", "Zinc"} };
                for (int i = 0; i < boiledPotatoNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "cartofi"),
                        Quantity = Convert.ToDouble(boiledPotatoNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(boiledPotatoNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(boiledPotatoNutrients[i, 1]))
                    });
                }

                object[,] onionNutrients = new object[,] { { 87.86, "g", "Apă" }, { 42, "kcal", "Aport caloric" }, { 9.56, "g", "Carbohidrați"}, { 1.4, "g", "Fibre"}, {0.85, "g", "Glucide"},
                                                           { 0.19, "g", "Lipide" }, {1.36, "g", "Proteine"}, {0.0042, "mg", "B1 - Tiamină"},
                                                           {0.023, "mg", "B2 - Riboflavină" }, {0.165, "mg", "B3 - Niacină"}, {0.113, "mg", "B5 - Acid pantotenic" },
                                                           {0.129, "mg", "B6"}, {5.2, "mg", "C"}, {15, "mcg", "B9 - Acid folic"}, {22, "mg", "Calciu" }, {0.067, "mg", "Cupru"},
                                                           {0.24, "mg", "Fier" }, {35, "mg", "Fosfor" }, {11, "mg", "Magneziu"}, {0.153, "mg", "Mangan"}, {166, "mg", "Potasiu"},
                                                           {0.6, "mcg", "Seleniu"}, {239, "mg", "Sodiu"}, {0.21, "mg", "Zinc"}, {6.8, "mg", "B4 - Colină"} };

                for (int i = 0; i < onionNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "ceapă gătită"),
                        Quantity = Convert.ToDouble(onionNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(onionNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(onionNutrients[i, 1]))
                    });
                }

                object[,] cookedCarrotNutrients = new object[,] { { 90.2, "g", "Apă" }, { 35, "kcal", "Aport caloric" }, { 8.2, "g", "Carbohidrați"}, { 3, "g", "Fibre"}, {3.5, "g", "Glucide"},
                    { 0.2, "g", "Lipide" }, {1, "g", "Proteine"}, {0.1, "mg", "B1 - Tiamină"}, {1, "mg", "E Alfa-tocoferol"}, {13, "mcg", "K - Filochinonă" },
                    {0, "mg", "B2 - Riboflavină" }, {0.6, "mg", "B3 - Niacină"}, {0.2, "mg", "B5 - Acid pantotenic" },
                    {0.2, "mg", "B6"}, {3.6, "mg", "C"}, {2, "mcg", "B9 - Acid folic"}, {30, "mg", "Calciu" }, {0, "mg", "Cupru"},
                    {0.3, "mg", "Fier" }, {30, "mg", "Fosfor" }, {10, "mg", "Magneziu"}, {0.2, "mg", "Mangan"}, {235, "mg", "Potasiu"},
                    {0.7, "mcg", "Seleniu"}, {302, "mg", "Sodiu"}, {0.2, "mg", "Zinc"}, {8.8, "mg", "B4 - Colină"} };
                for (int i = 0; i < cookedCarrotNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "morcovi gătiți"),
                        Quantity = Convert.ToDouble(cookedCarrotNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(cookedCarrotNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(cookedCarrotNutrients[i, 1]))
                    });
                }

                object[,] carrotNutrients = new object[,] { { 88.29, "g", "Apă" }, { 41, "kcal", "Aport caloric" }, { 9.58, "g", "Carbohidrați"}, { 2.8, "g", "Fibre"}, {4.74, "g", "Glucide"},
                    { 0.24, "g", "Lipide" }, {0.93, "g", "Proteine"}, {0.066, "mg", "B1 - Tiamină"}, {0.66, "mg", "E Alfa-tocoferol"}, {13.2, "mcg", "K - Filochinonă" },
                    {0.058, "mg", "B2 - Riboflavină" }, {0.983, "mg", "B3 - Niacină"}, {0.273, "mg", "B5 - Acid pantotenic" },
                    {0.138, "mg", "B6"}, {5.9, "mg", "C"}, {19, "mcg", "B9 - Acid folic"}, {33, "mg", "Calciu" }, {0.045, "mg", "Cupru"},
                    {3.2, "mg", "Fier" }, {35, "mg", "Fosfor" }, {12, "mg", "Magneziu"}, {0.143, "mg", "Mangan"}, {320, "mg", "Potasiu"},
                    {0.1, "mcg", "Seleniu"}, {69, "mg", "Sodiu"}, {0.24, "mg", "Zinc"}, {8.8, "mg", "B4 - Colină"} };
                for (int i = 0; i < carrotNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "morcovi"),
                        Quantity = Convert.ToDouble(carrotNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(carrotNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(carrotNutrients[i, 1]))
                    });
                }

                object[,] eggPlantNutrients = new object[,] { { 89.7, "g", "Apă" }, { 33, "kcal", "Aport caloric" }, { 8.1, "g", "Carbohidrați"}, { 2.5, "g", "Fibre"}, {3.2, "g", "Glucide"},
                    { 0.2, "g", "Lipide" }, {0.9, "g", "Proteine"}, {0.1, "mg", "B1 - Tiamină"}, {2.9, "mcg", "K - Filochinonă" },
                    {0, "mg", "B2 - Riboflavină" }, {0.6, "mg", "B3 - Niacină"}, {0.1, "mg", "B5 - Acid pantotenic" }, {0.4, "mg", "E Alfa-tocoferol"},
                    {0.1, "mg", "B6"}, {1.3, "mg", "C"}, {14, "mcg", "B9 - Acid folic"}, {6, "mg", "Calciu" }, {0.1, "mg", "Cupru"},
                    {0.3, "mg", "Fier" }, {15, "mg", "Fosfor" }, {11, "mg", "Magneziu"}, {0.1, "mg", "Mangan"}, {123, "mg", "Potasiu"},
                    {0.1, "mcg", "Seleniu"}, {239, "mg", "Sodiu"}, {0.1, "mg", "Zinc"}, {9.4, "mg", "B4 - Colină"} };
                for (int i = 0; i < eggPlantNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "vinete gătite"),
                        Quantity = Convert.ToDouble(eggPlantNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(eggPlantNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(eggPlantNutrients[i, 1]))
                    });
                }

                object[,] bananaNutrients = new object[,] { { 74.91, "g", "Apă" }, { 89, "kcal", "Aport caloric" }, { 22.84, "g", "Carbohidrați"}, { 2.6, "g", "Fibre"}, {12.23, "g", "Glucide"},
                    {0.33, "g", "Lipide" }, {1.09, "g", "Proteine"}, {0.031, "mg", "B1 - Tiamină"}, {0.5, "mcg", "K - Filochinonă" },
                    {0.073, "mg", "B2 - Riboflavină" }, {0.665, "mg", "B3 - Niacină"}, {0.334, "mg", "B5 - Acid pantotenic" }, {0.1, "mg", "E Alfa-tocoferol"},
                    {0.367, "mg", "B6"}, {8.7, "mg", "C"}, {20, "mcg", "B9 - Acid folic"}, {5, "mg", "Calciu" }, {0.078, "mg", "Cupru"},
                    {0.26, "mg", "Fier" }, {22, "mg", "Fosfor" }, {27, "mg", "Magneziu"}, {0.27, "mg", "Mangan"}, {358, "mg", "Potasiu"},
                    {1, "mcg", "Seleniu"}, {1, "mg", "Sodiu"}, {0.15, "mg", "Zinc"}, {9.8, "mg", "B4 - Colină"} };

                for (int i = 0; i < bananaNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "banane"),
                        Quantity = Convert.ToDouble(bananaNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(bananaNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(bananaNutrients[i, 1]))
                    });
                }

                object[,] greenPepperNutrients = new object[,] { {91.87, "g", "Apă" }, { 26, "kcal", "Aport caloric" }, { 6.11, "g", "Carbohidrați"}, { 1.2, "g", "Fibre"}, {3.19, "g", "Glucide"},
                                                           { 0.2, "g", "Lipide" }, {0.92, "g", "Proteine"}, {0.059, "mg", "B1 - Tiamină"},
                                                           {0.03, "mg", "B2 - Riboflavină" }, {0.477, "mg", "B3 - Niacină"}, {0.079, "mg", "B5 - Acid pantotenic" },
                                                           {0.233, "mg", "B6"}, {74.4, "mg", "C"}, {16, "mcg", "B9 - Acid folic"}, {9, "mg", "Calciu" }, {0.067, "mg", "Cupru"},
                                                           {0.46, "mg", "Fier" }, {18, "mg", "Fosfor" }, {10, "mg", "Magneziu"}, {0.115, "mg", "Mangan"}, {166, "mg", "Potasiu"},
                                                           {0.3, "mcg", "Seleniu"}, {238, "mg", "Sodiu"}, {0.12, "mg", "Zinc"}, {7.4, "mg", "B4 - Colină"}, {9.8, "mcg", "K - Filochinonă" } };
                for (int i = 0; i < greenPepperNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "ardei gras verde gătit"),
                        Quantity = Convert.ToDouble(greenPepperNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(greenPepperNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(greenPepperNutrients[i, 1]))
                    });
                }
                object[,] redpepperNutrients = new object[,] {  {91.87, "g", "Apă" }, { 26, "kcal", "Aport caloric" }, { 6.11, "g", "Carbohidrați"}, { 1.2, "g", "Fibre"}, {4.39, "g", "Glucide"},
                                                           { 0.2, "g", "Lipide" }, {0.92, "g", "Proteine"}, {147, "mcg", "A - RAE" }, {0.059, "mg", "B1 - Tiamină"},
                                                           {0.03, "mg", "B2 - Riboflavină" }, {0.477, "mg", "B3 - Niacină"}, {0.079, "mg", "B5 - Acid pantotenic" },
                                                           {0.233, "mg", "B6"}, {74.4, "mg", "C"}, {16, "mcg", "B9 - Acid folic"}, {9, "mg", "Calciu" }, {0.067, "mg", "Cupru"},
                                                           {0.46, "mg", "Fier" }, {18, "mg", "Fosfor" }, {10, "mg", "Magneziu"}, {0.115, "mg", "Mangan"}, {166, "mg", "Potasiu"},
                                                           {0.3, "mcg", "Seleniu"}, {238, "mg", "Sodiu"}, {0.12, "mg", "Zinc"}, {5.8, "mg", "B4 - Colină"}, {5.1, "mcg", "K - Filochinonă" } };
                for (int i = 0; i < redpepperNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "gogoșari gătiți"),
                        Quantity = Convert.ToDouble(redpepperNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(redpepperNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(redpepperNutrients[i, 1]))
                    });
                }

                object[,] parsnipNutrients = new object[,] { {80.24, "g", "Apă" }, { 71, "kcal", "Aport caloric" }, { 17.7, "g", "Carbohidrați"}, { 3.1, "g", "Fibre"}, {4.8, "g", "Glucide"},
                                                           { 0.3, "g", "Lipide" }, {1.32, "g", "Proteine"}, {147, "mcg", "A - RAE" }, {0.059, "mg", "B1 - Tiamină"},
                                                           {0.03, "mg", "B2 - Riboflavină" }, {0.477, "mg", "B3 - Niacină"}, {0.079, "mg", "B5 - Acid pantotenic" },
                                                           {0.233, "mg", "B6"}, {13, "mg", "C"}, {58, "mcg", "B9 - Acid folic"}, {37, "mg", "Calciu" }, {0.138, "mg", "Cupru"},
                                                           {0.58, "mg", "Fier" }, {69, "mg", "Fosfor" }, {29, "mg", "Magneziu"}, {0.0294, "mg", "Mangan"}, {397, "mg", "Potasiu"},
                                                           {1.7, "mcg", "Seleniu"}, {10, "mg", "Sodiu"}, {0.26, "mg", "Zinc"}, {27, "mg", "B4 - Colină"}, {1, "mcg", "K - Filochinonă" } };
                for (int i = 0; i < parsnipNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "păstârnac gătit"),
                        Quantity = Convert.ToDouble(parsnipNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(parsnipNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(parsnipNutrients[i, 1]))
                    });
                }

                object[,] pastaNutrients = new object[,] { { 68.56, "g", "Apă" }, { 131, "kcal", "Aport caloric" }, { 24.93, "g", "Carbohidrați"}, {5.15, "g", "Glucide"},
                                                                  { 1.05, "g", "Lipide" }, {5.15, "g", "Proteine"}, {6, "mcg", "A - RAE" }, {0.209, "mg", "B1 - Tiamină"},
                                                                  {0.15, "mg", "B2 - Riboflavină" }, {0.9, "mg", "B3 - Niacină"}, {0.193, "mg", "B5 - Acid pantotenic" }, {0.34, "mg", "B6"},
                                                                  {104, "mcg", "B9 - Acid folic"}, {6, "mg", "Calciu" }, {0.093, "mg", "Cupru"}, {1.14, "mg", "Fier" },
                                                                  {63, "mg", "Fosfor" }, {18, "mg", "Magneziu"}, {0.224, "mg", "Mangan"}, {24, "mg", "Potasiu"},
                                                                  {6, "mg", "Sodiu"}, {0.56, "mg", "Zinc"} };
                for (int i = 0; i < pastaNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "paste"),
                        Quantity = Convert.ToDouble(pastaNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(pastaNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(pastaNutrients[i, 1]))
                    });
                }

                object[,] soiaNutrients = new object[,] { {50.24, "g", "Apă" }, { 333, "kcal", "Aport caloric" }, { 29.2, "g", "Carbohidrați"}, { 16.7, "g", "Fibre"}, {4.8, "g", "Glucide"},
                                                           { 6, "g", "Lipide" }, {50, "g", "Proteine"}, {85, "mcg", "A - RAE" }, {0.059, "mg", "B1 - Tiamină"},
                                                           {0.03, "mg", "B2 - Riboflavină" }, {0.477, "mg", "B3 - Niacină"}, {0.079, "mg", "B5 - Acid pantotenic" },
                                                           {0.233, "mg", "B6"}, {13, "mg", "C"}, {58, "mcg", "B9 - Acid folic"}, {333, "mg", "Calciu" }, {0.138, "mg", "Cupru"},
                                                           {0.58, "mg", "Fier" }, {708, "mg", "Fosfor" }, {262, "mg", "Magneziu"}, {0.0294, "mg", "Mangan"}, {397, "mg", "Potasiu"},
                                                           {3, "mcg", "Seleniu"}, {10, "mg", "Sodiu"}, {4, "mg", "Zinc"}};
                for (int i = 0; i < soiaNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "soia texturat"),
                        Quantity = Convert.ToDouble(soiaNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(soiaNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(soiaNutrients[i, 1]))
                    });
                }

                object[,] tomatoSouceNutrients = new object[,] { { 80.63, "g", "Apă" }, { 79, "kcal", "Aport caloric" }, { 13.05, "g", "Carbohidrați"}, { 1.7, "g", "Fibre"}, {2.49, "g", "Glucide"},
                                                                  { 2.68, "g", "Lipide" }, {1.96, "g", "Proteine"}, {33, "mcg", "A - RAE" }, {0.108, "mg", "B1 - Tiamină"},
                                                                  {0.08, "mg", "B2 - Riboflavină" }, {1.11, "mg", "B3 - Niacină"}, {0.256, "mg", "B5 - Acid pantotenic" }, {0.086, "mg", "B6"},
                                                                  {18.2, "mg", "C"}, {11, "mcg", "B9 - Acid folic"}, {26, "mg", "Calciu" }, {0.095, "mg", "Cupru"}, {1.06, "mg", "Fier" },
                                                                  {38, "mg", "Fosfor" }, {15, "mg", "Magneziu"}, {0.193, "mg", "Mangan"}, {247, "mg", "Potasiu"}, {1.2, "mcg", "Seleniu"},
                                                                  {455, "mg", "Sodiu"}, {0.18, "mg", "Zinc"} };
                for (int i = 0; i < tomatoSouceNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "sos de tomate"),
                        Quantity = Convert.ToDouble(tomatoSouceNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(tomatoSouceNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(tomatoSouceNutrients[i, 1]))
                    });
                }

                object[,] cookedCaluliflowerNutrients = new object[,] { { 93, "g", "Apă" }, { 23, "kcal", "Aport caloric" }, { 4.11, "g", "Carbohidrați"}, { 2.3, "g", "Fibre"}, {1.86, "g", "Glucide"},
                    {0.45, "g", "Lipide" }, {1.84, "g", "Proteine"}, {1, "mcg", "A - RAE" }, {0.042, "mg", "B1 - Tiamină"},
                    {0.07, "mg", "B2 - Riboflavină" }, {0.41, "mg", "B3 - Niacină"}, {0.508, "mg", "B5 - Acid pantotenic" }, {0.173, "mg", "B6"},
                    {44.3, "mg", "C"}, {44, "mcg", "B9 - Acid folic"}, {16, "mg", "Calciu" }, {0.018, "mg", "Cupru"}, {0.32, "mg", "Fier" },{1, "mcg", "Fluor" },
                    {32, "mg", "Fosfor" }, {9, "mg", "Magneziu"}, {0.132, "mg", "Mangan"}, {142, "mg", "Potasiu"}, {0.6, "mcg", "Seleniu"},
                    {242, "mg", "Sodiu"}, {0.17, "mg", "Zinc"}, {39.1, "mg", "B4 - Colină"} };
                for (int i = 0; i < cookedCaluliflowerNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "conopidă gătită"),
                        Quantity = Convert.ToDouble(cookedCaluliflowerNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(cookedCaluliflowerNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(cookedCaluliflowerNutrients[i, 1]))
                    });
                }

                object[,] caluliflowerNutrients = new object[,] { { 92.07, "g", "Apă" }, { 25, "kcal", "Aport caloric" }, { 4.97, "g", "Carbohidrați"}, { 2, "g", "Fibre"}, {1.91, "g", "Glucide"},
                    {0.28, "g", "Lipide" }, {1.92, "g", "Proteine"}, {0, "mcg", "A - RAE" }, {0.05, "mg", "B1 - Tiamină"},
                    {0.06, "mg", "B2 - Riboflavină" }, {0.507, "mg", "B3 - Niacină"}, {0.667, "mg", "B5 - Acid pantotenic" }, {0.184, "mg", "B6"},
                    {48.2, "mg", "C"}, {57, "mcg", "B9 - Acid folic"}, {22, "mg", "Calciu" }, {0.039, "mg", "Cupru"}, {0.42, "mg", "Fier" },{1, "mcg", "Fluor" },
                    {44, "mg", "Fosfor" }, {15, "mg", "Magneziu"}, {0.155, "mg", "Mangan"}, {299, "mg", "Potasiu"}, {0.6, "mcg", "Seleniu"},
                    {30, "mg", "Sodiu"}, {0.27, "mg", "Zinc"}, {44.3, "mg", "B4 - Colină"} };
                for (int i = 0; i < caluliflowerNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "conopidă"),
                        Quantity = Convert.ToDouble(caluliflowerNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(caluliflowerNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(caluliflowerNutrients[i, 1]))
                    });
                }

                object[,] sfeclaNutrients = new object[,] { { 87.6, "g", "Apă" }, { 44, "kcal", "Aport caloric" }, { 9.96, "g", "Carbohidrați"}, { 2, "g", "Fibre"}, {7.96, "g", "Glucide"},
                    {0.18, "g", "Lipide" }, {1.68, "g", "Proteine"}, {2, "mcg", "A - RAE" }, {0.027, "mg", "B1 - Tiamină"}, {0.04, "mg", "E Alfa-tocoferol"},
                    {0.04, "mg", "B2 - Riboflavină" }, {0.331, "mg", "B3 - Niacină"}, {0.145, "mg", "B5 - Acid pantotenic" }, {0.067, "mg", "B6"},
                    {3.6, "mg", "C"}, {80, "mcg", "B9 - Acid folic"}, {16, "mg", "Calciu" }, {0.074, "mg", "Cupru"}, {0.79, "mg", "Fier" },{0, "mcg", "Fluor" },
                    {38, "mg", "Fosfor" }, {23, "mg", "Magneziu"}, {0.326, "mg", "Mangan"}, {305, "mg", "Potasiu"}, {0.7, "mcg", "Seleniu"},
                    {285, "mg", "Sodiu"}, {0.35, "mg", "Zinc"}, {6.3, "mg", "B4 - Colină"} };
                for (int i = 0; i < sfeclaNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "sfeclă gătită"),
                        Quantity = Convert.ToDouble(sfeclaNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(sfeclaNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(sfeclaNutrients[i, 1]))
                    });
                }

                object[,] brusselSproutsNutrients = new object[,] { { 88.9, "g", "Apă" }, { 36, "kcal", "Aport caloric" }, { 7.1, "g", "Carbohidrați"}, { 2.6, "g", "Fibre"}, {1.7, "g", "Glucide"},
                    {0.5, "g", "Lipide" }, {2.5, "g", "Proteine"}, {200, "mcg", "A - RAE" }, {0.1, "mg", "B1 - Tiamină"}, {0.4, "mg", "E Alfa-tocoferol"},
                    {0.1, "mg", "B2 - Riboflavină" }, {0.6, "mg", "B3 - Niacină"}, {0.3, "mg", "B5 - Acid pantotenic" }, {0.2, "mg", "B6"},
                    {62, "mg", "C"}, {60, "mcg", "B9 - Acid folic"}, {36, "mg", "Calciu" }, {0.1, "mg", "Cupru"}, {1.2, "mg", "Fier" },{0, "mcg", "Fluor" },
                    {56, "mg", "Fosfor" }, {20, "mg", "Magneziu"}, {0.2, "mg", "Mangan"}, {317, "mg", "Potasiu"}, {1.5, "mcg", "Seleniu"},
                    {257, "mg", "Sodiu"}, {0.3, "mg", "Zinc"}, {40.6, "mg", "B4 - Colină"} };
                for (int i = 0; i < brusselSproutsNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "varză de bruxelles gătită"),
                        Quantity = Convert.ToDouble(brusselSproutsNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(brusselSproutsNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(brusselSproutsNutrients[i, 1]))
                    });
                }

                object[,] oliveOilNutrients = new object[,] { { 0, "g", "Apă" }, { 884, "kcal", "Aport caloric" }, { 0, "g", "Carbohidrați"}, { 0, "g", "Fibre"}, {0, "g", "Glucide"},
                    {100, "g", "Lipide" }, {0, "g", "Proteine"}, {0, "mcg", "A - RAE" }, {0, "mg", "B1 - Tiamină"}, {14.3, "mg", "E Alfa-tocoferol"}, {60, "mcg", "K - Filochinonă" },
                    {0, "mg", "B2 - Riboflavină" }, {0, "mg", "B3 - Niacină"}, {0, "mg", "B5 - Acid pantotenic" }, {0, "mg", "B6"},
                    {0, "mg", "C"}, {0, "mcg", "B9 - Acid folic"}, {1, "mg", "Calciu" }, {0, "mg", "Cupru"}, {0.6, "mg", "Fier" },{0, "mcg", "Fluor" },
                    {0, "mg", "Fosfor" }, {0, "mg", "Magneziu"}, {0, "mg", "Mangan"}, {1, "mg", "Potasiu"}, {0, "mcg", "Seleniu"},
                    {2, "mg", "Sodiu"}, {0, "mg", "Zinc"}, {0.3, "mg", "B4 - Colină"} };
                for (int i = 0; i < oliveOilNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "ulei de măsline"),
                        Quantity = Convert.ToDouble(oliveOilNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(oliveOilNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(oliveOilNutrients[i, 1]))
                    });
                }

                object[,] coconutOilNutrients = new object[,] { { 0, "g", "Apă" }, { 862, "kcal", "Aport caloric" }, { 0, "g", "Carbohidrați"}, { 0, "g", "Fibre"}, {0, "g", "Glucide"},
                    {100, "g", "Lipide" }, {0, "g", "Proteine"}, {0, "mcg", "A - RAE" }, {0, "mg", "B1 - Tiamină"}, {0.1, "mg", "E Alfa-tocoferol"}, {0.5, "mcg", "K - Filochinonă" },
                    {0, "mg", "B2 - Riboflavină" }, {0, "mg", "B3 - Niacină"}, {0, "mg", "B5 - Acid pantotenic" }, {0, "mg", "B6"},
                    {0, "mg", "C"}, {0, "mcg", "B9 - Acid folic"}, {0, "mg", "Calciu" }, {0, "mg", "Cupru"}, {0, "mg", "Fier" },{0, "mcg", "Fluor" },
                    {0, "mg", "Fosfor" }, {0, "mg", "Magneziu"}, {0, "mg", "Mangan"}, {1, "mg", "Potasiu"}, {0, "mcg", "Seleniu"},
                    {2, "mg", "Sodiu"}, {0, "mg", "Zinc"}, {0.4, "mg", "B4 - Colină"} };
                for (int i = 0; i < coconutOilNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "ulei de cocos"),
                        Quantity = Convert.ToDouble(coconutOilNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(coconutOilNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(coconutOilNutrients[i, 1]))
                    });
                }

                object[,] sesamOilNutrients = new object[,] { { 0, "g", "Apă" }, { 884, "kcal", "Aport caloric" }, { 0, "g", "Carbohidrați"}, { 0, "g", "Fibre"}, {0, "g", "Glucide"},
                    {100, "g", "Lipide" }, {0, "g", "Proteine"}, {0, "mcg", "A - RAE" }, {0, "mg", "B1 - Tiamină"}, {1.4, "mg", "E Alfa-tocoferol"}, {13.96, "mcg", "K - Filochinonă" },
                    {0, "mg", "B2 - Riboflavină" }, {0, "mg", "B3 - Niacină"}, {0, "mg", "B5 - Acid pantotenic" }, {0, "mg", "B6"},
                    {0, "mg", "C"}, {0, "mcg", "B9 - Acid folic"}, {0, "mg", "Calciu" }, {0, "mg", "Cupru"}, {0, "mg", "Fier" },{0, "mcg", "Fluor" },
                    {0, "mg", "Fosfor" }, {0, "mg", "Magneziu"}, {0, "mg", "Mangan"}, {1, "mg", "Potasiu"}, {0, "mcg", "Seleniu"},
                    {2, "mg", "Sodiu"}, {0, "mg", "Zinc"}, {0.2, "mg", "B4 - Colină"} };
                for (int i = 0; i < sesamOilNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "ulei de susan"),
                        Quantity = Convert.ToDouble(sesamOilNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(sesamOilNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(sesamOilNutrients[i, 1]))
                    });
                }

                object[,] sunflowerOilNutrients = new object[,] { { 0, "g", "Apă" }, { 884, "kcal", "Aport caloric" }, { 0, "g", "Carbohidrați"}, { 0, "g", "Fibre"}, {0, "g", "Glucide"},
                    {100, "g", "Lipide" }, {0, "g", "Proteine"}, {0, "mcg", "A - RAE" }, {0, "mg", "B1 - Tiamină"}, {41.1, "mg", "E Alfa-tocoferol"}, {5.4, "mcg", "K - Filochinonă" },
                    {0, "mg", "B2 - Riboflavină" }, {0, "mg", "B3 - Niacină"}, {0, "mg", "B5 - Acid pantotenic" }, {0, "mg", "B6"},
                    {0, "mg", "C"}, {0, "mcg", "B9 - Acid folic"}, {0, "mg", "Calciu" }, {0, "mg", "Cupru"}, {0, "mg", "Fier" },{0, "mcg", "Fluor" },
                    {0, "mg", "Fosfor" }, {0, "mg", "Magneziu"}, {0, "mg", "Mangan"}, {1, "mg", "Potasiu"}, {0, "mcg", "Seleniu"},
                    {2, "mg", "Sodiu"}, {0, "mg", "Zinc"}, {0, "mg", "B4 - Colină"} };
                for (int i = 0; i < sunflowerOilNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "ulei de floarea soarelui"),
                        Quantity = Convert.ToDouble(sunflowerOilNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(sunflowerOilNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(sunflowerOilNutrients[i, 1]))
                    });
                }

                object[,] vegeatbleCreamNutrients = new object[,] { { 77.3, "g", "Apă" }, { 136, "kcal", "Aport caloric" }, { 11.4, "g", "Carbohidrați"}, { 0, "g", "Fibre"}, {11.4, "g", "Glucide"},
                    {10, "g", "Lipide" }, {1, "g", "Proteine"}, {0.2, "mcg", "A - RAE" }, {0, "mg", "B1 - Tiamină"}, {0.8, "mg", "E Alfa-tocoferol"}, {2.5, "mcg", "K - Filochinonă" },
                    {0, "mg", "B2 - Riboflavină" }, {0, "mg", "B3 - Niacină"}, {0, "mg", "B5 - Acid pantotenic" }, {0, "mg", "B6"},
                    {0, "mg", "C"}, {0, "mcg", "B9 - Acid folic"}, {9, "mg", "Calciu" }, {0, "mg", "Cupru"}, {0.0, "mg", "Fier" }, {0, "mcg", "Fluor" },
                    {64, "mg", "Fosfor" }, {0, "mg", "Magneziu"}, {0, "mg", "Mangan"}, {1, "mg", "Potasiu"}, {1.1, "mcg", "Seleniu"},
                    {79, "mg", "Sodiu"}, {0, "mg", "Zinc"}, {0, "mg", "B4 - Colină"} };
                for (int i = 0; i < vegeatbleCreamNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "frișcă vegetală"),
                        Quantity = Convert.ToDouble(vegeatbleCreamNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(vegeatbleCreamNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(vegeatbleCreamNutrients[i, 1]))
                    });
                }

                object[,] cashewNutrients = new object[,] { { 23.4, "g", "Apă" }, { 553, "kcal", "Aport caloric" }, { 30.19, "g", "Carbohidrați"}, { 3.3, "g", "Fibre"}, {5.91, "g", "Glucide"},
                    {43.85, "g", "Lipide" }, {18.22, "g", "Proteine"}, {0, "mcg", "A - RAE" }, {0.423, "mg", "B1 - Tiamină"}, {0.9, "mg", "E Alfa-tocoferol"}, {34.1, "mcg", "K - Filochinonă" },
                    {0.058, "mg", "B2 - Riboflavină" }, {1.062, "mg", "B3 - Niacină"}, {0.846, "mg", "B5 - Acid pantotenic" }, {0.417, "mg", "B6"},
                    {0.5, "mg", "C"}, {25, "mcg", "B9 - Acid folic"}, {37, "mg", "Calciu" }, {2.195, "mg", "Cupru"}, {6.68, "mg", "Fier" }, {0, "mcg", "Fluor" },
                    {593, "mg", "Fosfor" }, {292, "mg", "Magneziu"}, {1.655, "mg", "Mangan"}, {660, "mg", "Potasiu"}, {19.9, "mcg", "Seleniu"},
                    {12, "mg", "Sodiu"}, {5.78, "mg", "Zinc"}, {0, "mg", "B4 - Colină"} };
                for (int i = 0; i < cashewNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "caju"),
                        Quantity = Convert.ToDouble(cashewNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(cashewNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(cashewNutrients[i, 1]))
                    });
                }

                object[,] chiaNutrients = new object[,] { { 5.8, "g", "Apă" }, { 486, "kcal", "Aport caloric" }, { 42.12, "g", "Carbohidrați"}, { 34.4, "g", "Fibre"}, {0, "g", "Glucide"},
                    {30.74, "g", "Lipide" }, {16.54, "g", "Proteine"}, {1, "mcg", "A - RAE" }, {0.62, "mg", "B1 - Tiamină"}, {0.5, "mg", "E Alfa-tocoferol"}, {0, "mcg", "K - Filochinonă" },
                    {0.17, "mg", "B2 - Riboflavină" }, {8.83, "mg", "B3 - Niacină"}, {0, "mg", "B5 - Acid pantotenic" }, {0, "mg", "B6"},
                    {1.6, "mg", "C"}, {0, "mcg", "B9 - Acid folic"}, {631, "mg", "Calciu" }, {0.924, "mg", "Cupru"}, {7.72, "mg", "Fier" }, {0, "mcg", "Fluor" },
                    {860, "mg", "Fosfor" }, {335, "mg", "Magneziu"}, {2.723, "mg", "Mangan"}, {407, "mg", "Potasiu"}, {55.2, "mcg", "Seleniu"},
                    {16, "mg", "Sodiu"}, {4.58, "mg", "Zinc"} };
                for (int i = 0; i < chiaNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "seminţe de chia"),
                        Quantity = Convert.ToDouble(chiaNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(chiaNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(chiaNutrients[i, 1]))
                    });
                }

                object[,] soyMilkNutrients = new object[,] { { 88.05, "g", "Apă" }, { 54, "kcal", "Aport caloric" }, { 6.28, "g", "Carbohidrați"}, { 0.6, "g", "Fibre"}, {3.99, "g", "Glucide"},
                    {1.75, "g", "Lipide" }, {3.27, "g", "Proteine"}, {0.03, "mcg", "A - RAE" }, {0.06, "mg", "B1 - Tiamină"}, {0.11, "mg", "E Alfa-tocoferol"}, {3, "mcg", "K - Filochinonă" },
                    {0.069, "mg", "B2 - Riboflavină" }, {0.513, "mg", "B3 - Niacină"}, {0.373, "mg", "B5 - Acid pantotenic" }, {0.077, "mg", "B6"},
                    {18, "mcg", "B9 - Acid folic"}, {25, "mg", "Calciu" }, {0.128, "mg", "Cupru"}, {0.64, "mg", "Fier" }, {0, "mcg", "Fluor" },
                    {52, "mg", "Fosfor" }, {25, "mg", "Magneziu"}, {0.223, "mg", "Mangan"}, {118, "mg", "Potasiu"}, {4.8, "mcg", "Seleniu"},
                    {51, "mg", "Sodiu"}, {0.12, "mg", "Zinc"} };
                for (int i = 0; i < soyMilkNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "lapte de soia"),
                        Quantity = Convert.ToDouble(soyMilkNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(soyMilkNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(soyMilkNutrients[i, 1]))
                    });
                }

                object[,] sesameSeedsNutrients = new object[,] { { 4.69, "g", "Apă" }, { 573, "kcal", "Aport caloric" }, { 23.45, "g", "Carbohidrați"}, { 11.7, "g", "Fibre"}, {0.3, "g", "Glucide"},
                    { 46.9, "g", "Lipide" }, {17.73, "g", "Proteine"}, {0.79, "mg", "B1 - Tiamină"},
                    {0.247, "mg", "B2 - Riboflavină" }, {4.515, "mg", "B3 - Niacină"}, {0.05, "mg", "B5 - Acid pantotenic" },{25.6, "mg", "B4 - Colină"},
                    {97, "mcg", "B9 - Acid folic"}, {975, "mg", "Calciu" }, {4.095, "mg", "Cupru"}, {14.55, "mg", "Fier" },
                    {629, "mg", "Fosfor" }, {351, "mg", "Magneziu"}, {2.46, "mg", "Mangan"}, {468, "mg", "Potasiu"}, {34.4, "mcg", "Seleniu"},
                    {11, "mg", "Sodiu"}, {7.75, "mg", "Zinc"} };
                for (int i = 0; i < sesameSeedsNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "seminţe de susan"),
                        Quantity = Convert.ToDouble(sesameSeedsNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(sesameSeedsNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(sesameSeedsNutrients[i, 1]))
                    });
                }

                object[,] chickPeaNutrients = new object[,] { {66.78, "g", "Apă" }, { 138, "kcal", "Aport caloric" }, { 59.87, "g", "Carbohidrați"}, {17.4, "g", "Fibre"}, {10.7, "g", "Glucide"},
                    {6.04, "g", "Lipide" }, {19, "g", "Proteine"}, {3, "mcg", "A - RAE" }, {0.025, "mg", "B1 - Tiamină"},
                    {0.212, "mg", "B2 - Riboflavină" }, {1.125, "mg", "B3 - Niacină"}, {0.82, "mg", "E Alfa-tocoferol"}, {95.2, "mg", "B4 - Colină"},
                    {0.116, "mg", "B6"}, {3, "mg", "C"}, {41, "mcg", "B9 - Acid folic"}, {105, "mg", "Calciu" }, {0.847, "mg", "Cupru"},
                    {6.24, "mg", "Fier" }, {366, "mg", "Fosfor" }, {115, "mg", "Magneziu"}, {2.204, "mg", "Mangan"}, {875, "mg", "Potasiu"},
                    {8.2, "mcg", "Seleniu"}, {24, "mg", "Sodiu"}, {3.43, "mg", "Zinc"}};
                for (int i = 0; i < chickPeaNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "năut"),
                        Quantity = Convert.ToDouble(chickPeaNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(chickPeaNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(chickPeaNutrients[i, 1]))
                    });
                }

                object[,] lentilsNutrients = new object[,] { {69.4, "g", "Apă" }, { 116, "kcal", "Aport caloric" }, { 20.3, "g", "Carbohidrați"}, { 7.9, "g", "Fibre"}, {1.8, "g", "Glucide"},
                    {0.38, "g", "Lipide" }, {9.22, "g", "Proteine"}, {0.2, "mcg", "A - RAE" }, {0.169, "mg", "B1 - Tiamină"},
                    {0.073, "mg", "B2 - Riboflavină" }, {1.06, "mg", "B3 - Niacină"}, {0.11, "mg", "E Alfa-tocoferol"},
                    {0.178, "mg", "B6"}, {1.5, "mg", "C"}, {181, "mcg", "B9 - Acid folic"}, {19, "mg", "Calciu" }, {0.251, "mg", "Cupru"},
                    {3.33, "mg", "Fier" }, {180, "mg", "Fosfor" }, {36, "mg", "Magneziu"}, {0.494, "mg", "Mangan"}, {369, "mg", "Potasiu"},
                    {2.8, "mcg", "Seleniu"}, {2, "mg", "Sodiu"}, {1.27, "mg", "Zinc"}, {32.7, "mg", "B4 - Colină"}};
                for (int i = 0; i < lentilsNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "linte verde"),
                        Quantity = Convert.ToDouble(lentilsNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(lentilsNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(lentilsNutrients[i, 1]))
                    });
                }

                object[,] peasNutrients = new object[,] { {74.37, "g", "Apă" }, { 98, "kcal", "Aport caloric" }, { 17.08, "g", "Carbohidrați"}, {0, "g", "Fibre"}, { 5.5, "g", "Glucide"},
                    {0.51, "g", "Lipide" }, {7.05, "g", "Proteine"}, {5, "mcg", "A - RAE" }, {0.216, "mg", "B1 - Tiamină"},
                    {0.285, "mg", "B2 - Riboflavină" }, {1.072, "mg", "B3 - Niacină"}, {0, "mg", "E Alfa-tocoferol"},
                    {0.128, "mg", "B6"}, {6.6, "mg", "C"}, {36, "mcg", "B9 - Acid folic"}, {26, "mg", "Calciu" }, {0.02, "mg", "Cupru"},
                    {1.67, "mg", "Fier" }, {24, "mg", "Fosfor" }, {41, "mg", "Magneziu"}, {0.325, "mg", "Mangan"}, {268, "mg", "Potasiu"},
                    {0.6, "mcg", "Seleniu"}, {3, "mg", "Sodiu"}, {0.78, "mg", "Zinc"}, {0, "mg", "B4 - Colină"}};
                for (int i = 0; i < peasNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "mazăre boabe gătită"),
                        Quantity = Convert.ToDouble(peasNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(peasNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(peasNutrients[i, 1]))
                    });
                }

                object[,] parsleyNutrients = new object[,] { { 87.71, "g", "Apă" }, { 36, "kcal", "Aport caloric" }, { 6.33, "g", "Carbohidrați"}, { 3.3, "g", "Fibre"}, {0.85, "g", "Glucide"},
                    { 0.79, "g", "Lipide" }, {2.97, "g", "Proteine"}, {0.086, "mg", "B1 - Tiamină"}, {0.75, "mg", "E Alfa-tocoferol"},
                    {0.098, "mg", "B2 - Riboflavină" }, {1.313, "mg", "B3 - Niacină"}, {0.4, "mg", "B5 - Acid pantotenic" }, {1640, "mcg", "K - Filochinonă" },
                    {0.09, "mg", "B6"}, {133, "mg", "C"}, {152, "mcg", "B9 - Acid folic"}, {138, "mg", "Calciu" }, {0.149, "mg", "Cupru"},
                    {6.2, "mg", "Fier" }, {58, "mg", "Fosfor" }, {50, "mg", "Magneziu"}, {0.16, "mg", "Mangan"}, {554, "mg", "Potasiu"},
                    {0.1, "mcg", "Seleniu"}, {56, "mg", "Sodiu"}, {1.07, "mg", "Zinc"}, {12.8, "mg", "B4 - Colină"} };
                for (int i = 0; i < parsleyNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "pătrunjel"),
                        Quantity = Convert.ToDouble(parsleyNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(parsleyNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(parsleyNutrients[i, 1]))
                    });
                }

                object[,] garlicNutrients = new object[,] { { 58.6, "g", "Apă" }, { 149, "kcal", "Aport caloric" }, { 33.1, "g", "Carbohidrați"}, { 2.1, "g", "Fibre"}, {1.0, "g", "Glucide"},
                    {0.5, "g", "Lipide" }, {6.4, "g", "Proteine"}, {0.2, "mg", "B1 - Tiamină"}, {0.1, "mg", "E Alfa-tocoferol"},
                    {0.1, "mg", "B2 - Riboflavină" }, {0.7, "mg", "B3 - Niacină"}, {0.6, "mg", "B5 - Acid pantotenic" }, {1.7, "mcg", "K - Filochinonă" },
                    {1.2, "mg", "B6"}, {31.2, "mg", "C"}, {3.0, "mcg", "B9 - Acid folic"}, {181, "mg", "Calciu" }, {0.3, "mg", "Cupru"},
                    {1.7, "mg", "Fier" }, {153, "mg", "Fosfor" }, {25, "mg", "Magneziu"}, {1.7, "mg", "Mangan"}, {401, "mg", "Potasiu"},
                    {14.2, "mcg", "Seleniu"}, {17.0, "mg", "Sodiu"}, {1.2, "mg", "Zinc"}, {23.2, "mg", "B4 - Colină"} };
                for (int i = 0; i < garlicNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "usturoi"),
                        Quantity = Convert.ToDouble(garlicNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(garlicNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(garlicNutrients[i, 1]))
                    });
                }

                object[,] cucumberNutrients = new object[,] { { 95.23, "g", "Apă" }, { 15, "kcal", "Aport caloric" }, { 3.63, "g", "Carbohidrați"}, { 0.5, "g", "Fibre"}, {1.67, "g", "Glucide"},
                    {0.11, "g", "Lipide" }, {0.65, "g", "Proteine"}, {0.027, "mg", "B1 - Tiamină"}, {0.03, "mg", "E Alfa-tocoferol"},
                    {0.033, "mg", "B2 - Riboflavină" }, {0.098, "mg", "B3 - Niacină"}, {0.259, "mg", "B5 - Acid pantotenic" }, {16.4, "mcg", "K - Filochinonă" },
                    {0.04, "mg", "B6"}, {2.8, "mg", "C"}, {7, "mcg", "B9 - Acid folic"}, {16, "mg", "Calciu" }, {0.041, "mg", "Cupru"},
                    {0.28, "mg", "Fier" }, {24, "mg", "Fosfor" }, {13, "mg", "Magneziu"}, {0.079, "mg", "Mangan"}, {147, "mg", "Potasiu"},
                    {0.3, "mcg", "Seleniu"}, {2, "mg", "Sodiu"}, {0.2, "mg", "Zinc"}, {6, "mg", "B4 - Colină"} };
                for (int i = 0; i < cucumberNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "castraveți"),
                        Quantity = Convert.ToDouble(cucumberNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(cucumberNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(cucumberNutrients[i, 1]))
                    });
                }

                object[,] mararNutrients = new object[,] { { 85.89, "g", "Apă" }, { 43, "kcal", "Aport caloric" }, { 7.02, "g", "Carbohidrați"}, { 2.1, "g", "Fibre"}, {0, "g", "Glucide"},
                    { 1.12, "g", "Lipide" }, {3.46, "g", "Proteine"}, {0.058, "mg", "B1 - Tiamină"}, {0, "mg", "E Alfa-tocoferol"},
                    {0.296, "mg", "B2 - Riboflavină" }, {1.57, "mg", "B3 - Niacină"}, {0.397, "mg", "B5 - Acid pantotenic" }, {0, "mcg", "K - Filochinonă" },
                    {0.185, "mg", "B6"}, {85, "mg", "C"}, {150, "mcg", "B9 - Acid folic"}, {208, "mg", "Calciu" }, {0.146, "mg", "Cupru"},
                    {6.59, "mg", "Fier" }, {66, "mg", "Fosfor" }, {55, "mg", "Magneziu"}, {1.264, "mg", "Mangan"}, {738, "mg", "Potasiu"},
                    {0, "mcg", "Seleniu"}, {61, "mg", "Sodiu"}, {0.91, "mg", "Zinc"}, {0, "mg", "B4 - Colină"} };
                for (int i = 0; i < mararNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "mărar"),
                        Quantity = Convert.ToDouble(mararNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(mararNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(mararNutrients[i, 1]))
                    });
                }

                object[,] spinachNutrients = new object[,] { { 92.2, "g", "Apă" }, { 22, "kcal", "Aport caloric" }, { 3.9, "g", "Carbohidrați"}, { 2.8, "g", "Fibre"}, {0, "g", "Glucide"},
                    { 0.3, "g", "Lipide" }, {2.2, "g", "Proteine"}, {495, "mcg", "A - RAE" }, {0.068, "mg", "B1 - Tiamină"}, {0, "mg", "E Alfa-tocoferol"},
                    {0.093, "mg", "B2 - Riboflavină" }, {0.678, "mg", "B3 - Niacină"}, {0.178, "mg", "B5 - Acid pantotenic" }, {0, "mcg", "K - Filochinonă" },
                    {0.153, "mg", "B6"}, {130, "mg", "C"}, {159, "mcg", "B9 - Acid folic"}, {210, "mg", "Calciu" }, {0.075, "mg", "Cupru"},
                    {1.5, "mg", "Fier" }, {28, "mg", "Fosfor" }, {11, "mg", "Magneziu"}, {0.407, "mg", "Mangan"}, {449, "mg", "Potasiu"},
                    {0.8, "mcg", "Seleniu"}, {21, "mg", "Sodiu"}, {0.17, "mg", "Zinc"}, {0, "mg", "B4 - Colină"} };
                for (int i = 0; i < spinachNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "spanac"),
                        Quantity = Convert.ToDouble(spinachNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(spinachNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(spinachNutrients[i, 1]))
                    });
                }

                object[,] cookedSpinachNutrients = new object[,] { { 91.21, "g", "Apă" }, { 23, "kcal", "Aport caloric" }, { 3.75, "g", "Carbohidrați"}, { 2.4, "g", "Fibre"}, {0.43, "g", "Glucide"},
                    { 0.26, "g", "Lipide" }, {2.97, "g", "Proteine"}, {524, "mcg", "A - RAE" }, {0.095, "mg", "B1 - Tiamină"}, {2.08, "mg", "E Alfa-tocoferol"},
                    {0.236, "mg", "B2 - Riboflavină" }, {0.49, "mg", "B3 - Niacină"}, {0.145, "mg", "B5 - Acid pantotenic" }, {493.6, "mcg", "K - Filochinonă" },
                    {0.242, "mg", "B6"}, {9.8, "mg", "C"}, {146, "mcg", "B9 - Acid folic"}, {136, "mg", "Calciu" }, {0.174, "mg", "Cupru"},
                    {3.57, "mg", "Fier" }, {56, "mg", "Fosfor" }, {37.8, "mcg", "Fluor" }, {87, "mg", "Magneziu"}, {0.935, "mg", "Mangan"}, {466, "mg", "Potasiu"},
                    {1.5, "mcg", "Seleniu"}, {306, "mg", "Sodiu"}, {0.76, "mg", "Zinc"}, {19.7, "mg", "B4 - Colină"} };
                for (int i = 0; i < cookedSpinachNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "spanac gătit"),
                        Quantity = Convert.ToDouble(cookedSpinachNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(cookedSpinachNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(cookedSpinachNutrients[i, 1]))
                    });
                }

                object[,] broccoliNutrients = new object[,] { { 90.69, "g", "Apă" }, { 28, "kcal", "Aport caloric" }, { 5.24, "g", "Carbohidrați"}, { 2.4, "g", "Fibre"}, {0, "g", "Glucide"},
                    { 0, "g", "Lipide" }, {2.98, "g", "Proteine"}, {150, "mcg", "A - RAE" }, {0.065, "mg", "B1 - Tiamină"}, {0, "mg", "E Alfa-tocoferol"},
                    {0.119, "mg", "B2 - Riboflavină" }, {0.638, "mg", "B3 - Niacină"}, {0.535, "mg", "B5 - Acid pantotenic" }, {0, "mcg", "K - Filochinonă" },
                    {0.159, "mg", "B6"}, {93.2, "mg", "C"}, {71, "mcg", "B9 - Acid folic"}, {48, "mg", "Calciu" }, {0.045, "mg", "Cupru"},
                    {0.88, "mg", "Fier" }, {66, "mg", "Fosfor" }, {0, "mcg", "Fluor" }, {25, "mg", "Magneziu"}, {0.229, "mg", "Mangan"}, {325, "mg", "Potasiu"},
                    {3, "mcg", "Seleniu"}, {27, "mg", "Sodiu"}, {0.4, "mg", "Zinc"}, {0, "mg", "B4 - Colină"} };
                for (int i = 0; i < broccoliNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "broccoli"),
                        Quantity = Convert.ToDouble(broccoliNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(broccoliNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(broccoliNutrients[i, 1]))
                    });
                }

                object[,] cookedBroccoliNutrients = new object[,] { { 89.25, "g", "Apă" }, { 35, "kcal", "Aport caloric" }, { 7.18, "g", "Carbohidrați"}, { 3.3, "g", "Fibre"}, {1.39, "g", "Glucide"},
                    {0.41, "g", "Lipide" }, {2.38, "g", "Proteine"}, {77, "mcg", "A - RAE" }, {0.063, "mg", "B1 - Tiamină"}, {1.45, "mg", "E Alfa-tocoferol"},
                    {0.123, "mg", "B2 - Riboflavină" }, {0.553, "mg", "B3 - Niacină"}, {0.616, "mg", "B5 - Acid pantotenic" }, {141.4, "mcg", "K - Filochinonă" },
                    {0.2, "mg", "B6"}, {64.9, "mg", "C"}, {208, "mcg", "B9 - Acid folic"}, {40, "mg", "Calciu" }, {0.061, "mg", "Cupru"},
                    {0.67, "mg", "Fier" }, {67, "mg", "Fosfor" }, {4, "mcg", "Fluor" }, {21, "mg", "Magneziu"}, {0.194, "mg", "Mangan"}, {293, "mg", "Potasiu"},
                    {1.6, "mcg", "Seleniu"}, {41, "mg", "Sodiu"}, {0.45, "mg", "Zinc"}, {40.1, "mg", "B4 - Colină"} };
                for (int i = 0; i < cookedBroccoliNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "broccoli gătit"),
                        Quantity = Convert.ToDouble(cookedBroccoliNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(cookedBroccoliNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(cookedBroccoliNutrients[i, 1]))
                    });
                }

                object[,] rucolaNutrients = new object[,] { {91.71, "g", "Apă" }, { 25, "kcal", "Aport caloric" }, { 3.65, "g", "Carbohidrați"}, { 1.6, "g", "Fibre"}, {2.05, "g", "Glucide"},
                    {0.66, "g", "Lipide" }, {2.58, "g", "Proteine"}, {119, "mcg", "A - RAE" }, {0.044, "mg", "B1 - Tiamină"}, {0.43, "mg", "E Alfa-tocoferol"},
                    {0.086, "mg", "B2 - Riboflavină" }, {0.305, "mg", "B3 - Niacină"}, {0.437, "mg", "B5 - Acid pantotenic" }, {108.6, "mcg", "K - Filochinonă" },
                    {0.073, "mg", "B6"}, {15, "mg", "C"}, {97, "mcg", "B9 - Acid folic"}, {160, "mg", "Calciu" }, {0.076, "mg", "Cupru"},
                    {1.46, "mg", "Fier" }, {52, "mg", "Fosfor" }, {0, "mcg", "Fluor" }, {47, "mg", "Magneziu"}, {0.321, "mg", "Mangan"}, {369, "mg", "Potasiu"},
                    {0.3, "mcg", "Seleniu"}, {27, "mg", "Sodiu"}, {0.47, "mg", "Zinc"}, {15.3, "mg", "B4 - Colină"} };
                for (int i = 0; i < rucolaNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "rucola"),
                        Quantity = Convert.ToDouble(rucolaNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(rucolaNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(rucolaNutrients[i, 1]))
                    });
                }

                object[,] corianderNutrients = new object[,] { { 92.2, "g", "Apă" }, { 23, "kcal", "Aport caloric" }, { 3.67, "g", "Carbohidrați"}, { 2.8, "g", "Fibre"}, {0.87, "g", "Glucide"},
                    { 0.52, "g", "Lipide" }, {2.13, "g", "Proteine"}, {0.067, "mg", "B1 - Tiamină"}, {2.5, "mg", "E Alfa-tocoferol"},
                    {0.162, "mg", "B2 - Riboflavină" }, {1.114, "mg", "B3 - Niacină"}, {0.57, "mg", "B5 - Acid pantotenic" }, {310, "mcg", "K - Filochinonă" },
                    {0.149, "mg", "B6"}, {27, "mg", "C"}, {62, "mcg", "B9 - Acid folic"}, {67, "mg", "Calciu" }, {0.225, "mg", "Cupru"},
                    {1.77, "mg", "Fier" }, {48, "mg", "Fosfor" }, {26, "mg", "Magneziu"}, {0.462, "mg", "Mangan"}, {521, "mg", "Potasiu"},
                    {0.9, "mcg", "Seleniu"}, {46, "mg", "Sodiu"}, {0.5, "mg", "Zinc"}, {12.8, "mg", "B4 - Colină"} };
                for (int i = 0; i < parsleyNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "coriandru"),
                        Quantity = Convert.ToDouble(parsleyNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(parsleyNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(parsleyNutrients[i, 1]))
                    });
                }

                object[,] wheatFlowerNutrients = new object[,] { { 11.92, "g", "Apă" }, { 364, "kcal", "Aport caloric" }, { 76.31, "g", "Carbohidrați"}, { 2.7, "g", "Fibre"}, {0.27, "g", "Glucide"},
                    { 0.98, "g", "Lipide" }, {10.33, "g", "Proteine"}, {0.06, "mg", "E Alfa-tocoferol"},
                    {0.494, "mg", "B2 - Riboflavină" }, {5.904, "mg", "B3 - Niacină"}, {0.438, "mg", "B5 - Acid pantotenic" }, {0.3, "mcg", "K - Filochinonă" },
                    {0.044, "mg", "B6"}, {291, "mcg", "B9 - Acid folic"}, {15, "mg", "Calciu" }, {0.144, "mg", "Cupru"},
                    {4.64, "mg", "Fier" }, {108, "mg", "Fosfor" }, {22, "mg", "Magneziu"}, {0.682, "mg", "Mangan"}, {107, "mg", "Potasiu"},
                    {33.8, "mcg", "Seleniu"}, {2, "mg", "Sodiu"}, {0.7, "mg", "Zinc"}, {10.4, "mg", "B4 - Colină"} };
                for (int i = 0; i < wheatFlowerNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "făină de grâu"),
                        Quantity = Convert.ToDouble(wheatFlowerNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(wheatFlowerNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(wheatFlowerNutrients[i, 1]))
                    });
                }

                object[,] riceNutrients = new object[,] { { 68.53, "g", "Apă" }, { 130, "kcal", "Aport caloric" }, { 28.73, "g", "Carbohidrați"},
                    { 0.19, "g", "Lipide" }, {2.36, "g", "Proteine"}, {0.02, "mg", "B1 - Tiamină"},
                    {0.016, "mg", "B2 - Riboflavină" }, {0.4, "mg", "B3 - Niacină"},
                    {0.059, "mg", "B6"}, {2, "mcg", "B9 - Acid folic"}, {1, "mg", "Calciu" }, {0.072, "mg", "Cupru"},
                    {0.2, "mg", "Fier" }, { 33, "mg", "Fosfor" }, {8, "mg", "Magneziu"}, {0.357, "mg", "Mangan"}, {26, "mg", "Potasiu"},
                    {0.4, "mg", "Zinc"} };
                for (int i = 0; i < riceNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "orez"),
                        Quantity = Convert.ToDouble(riceNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(riceNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(riceNutrients[i, 1]))
                    });
                }

                object[,] bulgurNutrients = new object[,] { {9, "g", "Apă" }, { 342, "kcal", "Aport caloric" }, { 75.87, "g", "Carbohidrați"}, {0.41, "g", "Glucide"},
                    {1.33, "g", "Lipide" }, {12.29, "g", "Proteine"}, {0, "mcg", "A - RAE" }, {1.9, "mcg", "K - Filochinonă" },
                    {0.115, "mg", "B2 - Riboflavină" }, {5.114, "mg", "B3 - Niacină"}, {0.06, "mg", "E Alfa-tocoferol"}, {28.1, "mg", "B4 - Colină"},
                    {0.342, "mg", "B6"}, {27, "mcg", "B9 - Acid folic"}, {35, "mg", "Calciu" }, {0.335, "mg", "Cupru"},
                    {2.46, "mg", "Fier" }, {300, "mg", "Fosfor" }, {164, "mg", "Magneziu"}, {3.048, "mg", "Mangan"}, {410, "mg", "Potasiu"},
                    {2.3, "mcg", "Seleniu"}, {17, "mg", "Sodiu"}, {1.93, "mg", "Zinc"}};
                for (int i = 0; i < bulgurNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "bulgur"),
                        Quantity = Convert.ToDouble(bulgurNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(bulgurNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(bulgurNutrients[i, 1]))
                    });
                }

                object[,] whiteQuinoaNutrients = new object[,] { {71.61, "g", "Apă" }, { 120, "kcal", "Aport caloric" }, {21.3, "g", "Carbohidrați"}, {0.87, "g", "Glucide"},
                    {1.92, "g", "Lipide" }, {4.4, "g", "Proteine"}, {0, "mcg", "A - RAE" }, {0, "mcg", "K - Filochinonă" },
                    {0.11, "mg", "B2 - Riboflavină" }, {0.412, "mg", "B3 - Niacină"}, {0.63, "mg", "E Alfa-tocoferol"},
                    {0.123, "mg", "B6"}, {42, "mcg", "B9 - Acid folic"}, {17, "mg", "Calciu" }, {0.192, "mg", "Cupru"},
                    {1.49, "mg", "Fier" }, {152, "mg", "Fosfor" }, {64, "mg", "Magneziu"}, {0.631, "mg", "Mangan"}, {172, "mg", "Potasiu"},
                    {2.8, "mcg", "Seleniu"}, {7, "mg", "Sodiu"}, {1.09, "mg", "Zinc"}, {23, "mg", "B4 - Colină"}};
                for (int i = 0; i < whiteQuinoaNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "quinoa albă gătită"),
                        Quantity = Convert.ToDouble(whiteQuinoaNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(whiteQuinoaNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(whiteQuinoaNutrients[i, 1]))
                    });
                }

                object[,] tofuNutrients = new object[,] { {70.9, "g", "Apă" }, { 151, "kcal", "Aport caloric" }, { 6.9, "g", "Carbohidrați"}, {1.6, "g", "Glucide"},
                    {8.1, "g", "Lipide" }, {12.5, "g", "Proteine"}, {2, "mcg", "A - RAE" }, {4.6, "mcg", "K - Filochinonă" },
                    {0.14, "mg", "B2 - Riboflavină" }, {0.5, "mg", "B3 - Niacină"}, {0.6, "mg", "E Alfa-tocoferol"}, {62.5, "mg", "B4 - Colină"},
                    {0.07, "mg", "B6"}, {22, "mcg", "B9 - Acid folic"}, {188, "mg", "Calciu" }, {0.38, "mg", "Cupru"},
                    {5.6, "mg", "Fier" }, {222, "mg", "Fosfor" }, {228, "mg", "Magneziu"}, {0.889, "mg", "Mangan"}, {199, "mg", "Potasiu"},
                    {16.8, "mcg", "Seleniu"}, {20, "mg", "Sodiu"}, {1.72, "mg", "Zinc"}};
                for (int i = 0; i < tofuNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "tofu"),
                        Quantity = Convert.ToDouble(tofuNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(tofuNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(tofuNutrients[i, 1]))
                    });
                }

                object[,] sparanghelNutrients = new object[,] { {93.22, "g", "Apă" }, { 20, "kcal", "Aport caloric" }, { 3.88, "g", "Carbohidrați"}, {1.88, "g", "Glucide"}, {2.1, "g", "Fibre"},
                    {0.12, "g", "Lipide" }, {2.2, "g", "Proteine"}, {38, "mcg", "A - RAE" }, {41.6, "mcg", "K - Filochinonă" },
                    {0.141, "mg", "B2 - Riboflavină" }, {0.978, "mg", "B3 - Niacină"}, {1.13, "mg", "E Alfa-tocoferol"}, {16, "mg", "B4 - Colină"},{ 0.274, "mg" , "B5 - Acid pantotenic"},
                    {0.091, "mg", "B6"}, {52, "mcg", "B9 - Acid folic"}, {24, "mg", "Calciu" }, {0.189, "mg", "Cupru"},
                    {2.14, "mg", "Fier" }, {52, "mg", "Fosfor" }, {14, "mg", "Magneziu"}, {0.158, "mg", "Mangan"}, {202, "mg", "Potasiu"},
                    {2.3, "mcg", "Seleniu"}, {2, "mg", "Sodiu"}, {0.54, "mg", "Zinc"}};
                for (int i = 0; i < sparanghelNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "sparanghel"),
                        Quantity = Convert.ToDouble(sparanghelNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(sparanghelNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(sparanghelNutrients[i, 1]))
                    });
                }

                object[,] soySauceNutrients = new object[,] { {68.02, "g", "Apă" }, { 60, "kcal", "Aport caloric" }, { 7.84, "g", "Carbohidrați"}, {1.3, "g", "Glucide"}, {0.5, "g", "Fibre"},
                    {0.51, "g", "Lipide" }, {7, "g", "Proteine"},
                    {0.109, "mg", "B2 - Riboflavină" }, {2.828, "mg", "B3 - Niacină"}, {27.5, "mg", "B4 - Colină"},{ 0.269, "mg" , "B5 - Acid pantotenic"},
                    {0.143, "mg", "B6"}, {13, "mcg", "B9 - Acid folic"}, {17, "mg", "Calciu" }, {0.041, "mg", "Cupru"},
                    {0.33, "mg", "Fier" }, {94, "mg", "Fosfor" }, {29, "mg", "Magneziu"}, {0.1, "mg", "Mangan"}, {447, "mg", "Potasiu"},
                    {0.8, "mcg", "Seleniu"}, {6820, "mg", "Sodiu"}, {0.23, "mg", "Zinc"}};
                for (int i = 0; i < soySauceNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "sos de soia"),
                        Quantity = Convert.ToDouble(soySauceNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(soySauceNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(soySauceNutrients[i, 1]))
                    });
                }

                object[,] silkenTofuNutrients = new object[,] { { 90, "g", "Apă" }, { 55, "kcal", "Aport caloric" }, { 2.9, "g", "Carbohidrați"}, { 0.1, "g", "Fibre"}, {1.3, "g", "Glucide"},
                    {2.7, "g", "Lipide" }, {4.8, "g", "Proteine"}, {0.1, "mg", "B1 - Tiamină"},
                    {0.0, "mg", "B2 - Riboflavină" }, {0.3, "mg", "B3 - Niacină"},
                    {0.0, "mg", "B6"}, {31, "mg", "Calciu" }, {0.2, "mg", "Cupru"},
                    {0.8, "mg", "Fier" }, {62, "mg", "Fosfor" }, {29, "mg", "Magneziu"},  {180, "mg", "Potasiu"},
                    {5, "mg", "Sodiu"}, {0.5, "mg", "Zinc"} };
                for (int i = 0; i < silkenTofuNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "silken tofu"),
                        Quantity = Convert.ToDouble(silkenTofuNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(silkenTofuNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(onionNutrients[i, 1]))
                    });
                }

                object[,] brownSugarNutrients = new object[,] { { 1.3, "g", "Apă" }, { 380, "kcal", "Aport caloric" }, { 98.1, "g", "Carbohidrați"},  {97, "g", "Glucide"},
                    {0.1, "g", "Proteine"}, {0.086, "mg", "B1 - Tiamină"},
                    {0.1, "mg", "B3 - Niacină"}, {0.4, "mg", "B5 - Acid pantotenic" },
                    {1, "mcg", "B9 - Acid folic"}, {83, "mg", "Calciu" },
                    {0.7, "mg", "Fier" }, {4, "mg", "Fosfor" }, {0.1, "mg", "Magneziu"}, {0.1, "mg", "Mangan"}, {133, "mg", "Potasiu"},
                    {1.2, "mcg", "Seleniu"}, {28, "mg", "Sodiu"}, {2.3, "mg", "B4 - Colină"} };
                for (int i = 0; i < brownSugarNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "zahăr brun"),
                        Quantity = Convert.ToDouble(brownSugarNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(brownSugarNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(brownSugarNutrients[i, 1]))
                    });
                }

                object[,] dateNutrients = new object[,] { { 20.53, "g", "Apă" }, { 282, "kcal", "Aport caloric" }, { 75.03, "g", "Carbohidrați"}, { 8, "g", "Fibre"}, {63.35, "g", "Glucide"},
                    {0.39, "g", "Lipide" }, {2.45, "g", "Proteine"},{3, "mcg", "A - RAE" }, {0.052, "mg", "B1 - Tiamină"}, {0.05, "mg", "E Alfa-tocoferol"},
                    {0.066, "mg", "B2 - Riboflavină" }, {1.274, "mg", "B3 - Niacină"}, {0.589, "mg", "B5 - Acid pantotenic" }, {2.7, "mcg", "K - Filochinonă" },
                    {0.156, "mg", "B6"}, {0.4, "mg", "C"}, {19, "mcg", "B9 - Acid folic"}, {39, "mg", "Calciu" }, {0.206, "mg", "Cupru"},
                    {1.02, "mg", "Fier" }, {62, "mg", "Fosfor" }, {43, "mg", "Magneziu"}, {0.262, "mg", "Mangan"}, {656, "mg", "Potasiu"},
                    {3, "mcg", "Seleniu"}, {2, "mg", "Sodiu"}, {0.29, "mg", "Zinc"}, {6.3, "mg", "B4 - Colină"} };
                for (int i = 0; i < dateNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "curmale"),
                        Quantity = Convert.ToDouble(dateNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(dateNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(dateNutrients[i, 1]))
                    });
                }

                object[,] peanutButterNutrients = new object[,] { { 1.8, "g", "Apă" }, { 588, "kcal", "Aport caloric" }, { 20, "g", "Carbohidrați"}, {6, "g", "Fibre"}, {9.2, "g", "Glucide"},
                    {50, "g", "Lipide" }, {25.1, "g", "Proteine"}, {0, "mcg", "A - RAE" }, {0.1, "mg", "B1 - Tiamină"}, {9.0, "mg", "E Alfa-tocoferol"},
                    {0.1, "mg", "B2 - Riboflavină" }, {13.4, "mg", "B3 - Niacină"}, {1.1, "mg", "B5 - Acid pantotenic" }, {0.6, "mcg", "K - Filochinonă" },
                    {0.5, "mg", "B6"}, {0, "mg", "C"}, {74, "mcg", "B9 - Acid folic"}, {43, "mg", "Calciu" }, {0.5, "mg", "Cupru"},
                    {1.9, "mg", "Fier" }, {358, "mg", "Fosfor" }, {154, "mg", "Magneziu"}, {1.5, "mg", "Mangan"}, {649, "mg", "Potasiu"},
                    {5.6, "mcg", "Seleniu"}, {459, "mg", "Sodiu"}, {2.9, "mg", "Zinc"}, {63, "mg", "B4 - Colină"} };
                for (int i = 0; i < peanutButterNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "unt de arahide"),
                        Quantity = Convert.ToDouble(peanutButterNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(peanutButterNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(peanutButterNutrients[i, 1]))
                    });
                }

                object[,] darkChocolateNutrients = new object[,] { { 1.37, "g", "Apă" }, { 598, "kcal", "Aport caloric" }, { 45.9, "g", "Carbohidrați"}, { 10.9, "g", "Fibre"}, {23.99, "g", "Glucide"},
                    {42.63, "g", "Lipide" }, {7.79, "g", "Proteine"}, {2, "mcg", "A - RAE" }, {0.034, "mg", "B1 - Tiamină"}, {0.59, "mg", "E Alfa-tocoferol"},
                    {0.078, "mg", "B2 - Riboflavină" }, {1.054, "mg", "B3 - Niacină"}, {0.418, "mg", "B5 - Acid pantotenic" }, {7.3, "mcg", "K - Filochinonă" },
                    {0.038, "mg", "B6"}, {73, "mg", "Calciu" }, {1.766, "mg", "Cupru"},
                    {11.9, "mg", "Fier" }, {308, "mg", "Fosfor" }, {228, "mg", "Magneziu"}, {1.948, "mg", "Mangan"}, {715, "mg", "Potasiu"},
                    {6.8, "mcg", "Seleniu"}, {20, "mg", "Sodiu"}, {3.31, "mg", "Zinc"} };
                for (int i = 0; i < darkChocolateNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "ciocolată amăruie > 70-85% cacao"),
                        Quantity = Convert.ToDouble(darkChocolateNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(darkChocolateNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(darkChocolateNutrients[i, 1]))
                    });
                }

                object[,] pickledCabbageNutrients = new object[,] { { 91, "g", "Apă" }, { 30, "kcal", "Aport caloric" }, { 5.7, "g", "Carbohidrați"}, { 3.1, "g", "Fibre"}, {1.3, "g", "Glucide"},
                    {0.9, "g", "Lipide" }, {1.6, "g", "Proteine"}, {5.84, "mcg", "A - RAE" }, {0, "mg", "B1 - Tiamină"}, {0.1, "mg", "E Alfa-tocoferol"},
                    {0, "mg", "B2 - Riboflavină" }, {0.2, "mg", "B3 - Niacină"}, {0.2, "mg", "B5 - Acid pantotenic" }, {126, "mcg", "K - Filochinonă" },
                    {0.1, "mg", "B6"}, {48, "mg", "Calciu" }, {0, "mg", "Cupru"},
                    {0.5, "mg", "Fier" }, {43, "mg", "Fosfor" }, {12, "mg", "Magneziu"}, {12, "mg", "Mangan"}, {853, "mg", "Potasiu"},
                    {1, "mcg", "Seleniu"}, {277, "mg", "Sodiu"}, {0.2, "mg", "Zinc"} };
                for (int i = 0; i < pickledCabbageNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "varză murată"),
                        Quantity = Convert.ToDouble(pickledCabbageNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(pickledCabbageNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(pickledCabbageNutrients[i, 1]))
                    });
                }

                object[,] lemonJuiceNutrients = new object[,] { { 92.31, "g", "Apă" }, { 22, "kcal", "Aport caloric" }, { 6.9, "g", "Carbohidrați"}, { 0.3, "g", "Fibre"}, {2.52, "g", "Glucide"},
                    {0.24, "g", "Lipide" }, {0.35, "g", "Proteine"}, {0, "mcg", "A - RAE" }, {0.024, "mg", "B1 - Tiamină"}, {0.15, "mg", "E Alfa-tocoferol"},
                    {0.015, "mg", "B2 - Riboflavină" }, {0.091, "mg", "B3 - Niacină"}, {0.131, "mg", "B5 - Acid pantotenic" }, {0, "mcg", "K - Filochinonă" },
                    {0.046, "mg", "B6"}, {38.7, "mg", "C"}, {20, "mcg", "B9 - Acid folic"}, {6, "mg", "Calciu" }, {0.116, "mg", "Cupru"},
                    {0.08, "mg", "Fier" }, {8, "mg", "Fosfor" }, {6, "mg", "Magneziu"}, {0.012, "mg", "Mangan"}, {103, "mg", "Potasiu"},
                    {0.1, "mcg", "Seleniu"}, {1, "mg", "Sodiu"}, {0.05, "mg", "Zinc"} };
                for (int i = 0; i < lemonJuiceNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "zeamă de lămâie"),
                        Quantity = Convert.ToDouble(lemonJuiceNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(lemonJuiceNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(lemonJuiceNutrients[i, 1]))
                    });
                }

                object[,] avocadoNutrients = new object[,] { { 73.2, "g", "Apă" }, { 160, "kcal", "Aport caloric" }, { 30.6, "g", "Carbohidrați"}, { 6.7, "g", "Fibre"}, {0.7, "g", "Glucide"},
                    {14.7, "g", "Lipide" }, {2, "g", "Proteine"}, {0, "mcg", "A - RAE" }, {0.1, "mg", "B1 - Tiamină"}, {2.1, "mg", "E Alfa-tocoferol"},
                    {0.1, "mg", "B2 - Riboflavină" }, {1.7, "mg", "B3 - Niacină"}, {1.4, "mg", "B5 - Acid pantotenic" }, {21, "mcg", "K - Filochinonă" },
                    {0.3, "mg", "B6"}, {0, "mg", "C"}, {81, "mcg", "B9 - Acid folic"}, {12, "mg", "Calciu" }, {0.2, "mg", "Cupru"},
                    {0.5, "mg", "Fier" }, {52, "mg", "Fosfor" }, {29, "mg", "Magneziu"}, {0.1, "mg", "Mangan"}, {485, "mg", "Potasiu"},
                    {0.4, "mcg", "Seleniu"}, {7, "mg", "Sodiu"}, {0.6, "mg", "Zinc"}, {14.2, "mg", "B4 - Colină"}};
                for (int i = 0; i < avocadoNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "avocado"),
                        Quantity = Convert.ToDouble(avocadoNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(avocadoNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(avocadoNutrients[i, 1]))
                    });
                }

                object[,] blackOliveNutrients = new object[,] { { 79.99, "g", "Apă" }, { 115, "kcal", "Aport caloric" }, { 6.26, "g", "Carbohidrați"}, { 3.2, "g", "Fibre"}, {0, "g", "Glucide"},
                    {10.68, "g", "Lipide" }, {0.84, "g", "Proteine"}, {0, "mcg", "A - RAE" }, {0.003, "mg", "B1 - Tiamină"}, {1.65, "mg", "E Alfa-tocoferol"},
                    {0, "mg", "B2 - Riboflavină" }, {0.037, "mg", "B3 - Niacină"}, {0.015, "mg", "B5 - Acid pantotenic" }, {1.4, "mcg", "K - Filochinonă" },
                    {0.009, "mg", "B6"}, {0.9, "mg", "C"}, {0, "mcg", "B9 - Acid folic"}, {88, "mg", "Calciu" }, {0.251, "mg", "Cupru"},
                    {3.3, "mg", "Fier" }, {3, "mg", "Fosfor" }, {4, "mg", "Magneziu"}, {0.02, "mg", "Mangan"}, {8, "mg", "Potasiu"},
                    {0.9, "mcg", "Seleniu"}, {735, "mg", "Sodiu"}, {0.22, "mg", "Zinc"}, {10.3, "mg", "B4 - Colină"}};
                for (int i = 0; i < blackOliveNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "măsline negre"),
                        Quantity = Convert.ToDouble(blackOliveNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(blackOliveNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(blackOliveNutrients[i, 1]))
                    });
                }

                object[,] tahiniNutrients = new object[,] { { 3, "g", "Apă" }, { 607, "kcal", "Aport caloric" }, { 17.89, "g", "Carbohidrați"}, { 9.3, "g", "Fibre"}, {0, "g", "Glucide"},
                    {56.44, "g", "Lipide" }, {17.95, "g", "Proteine"}, {3, "mcg", "A - RAE" }, {1.587, "mg", "B1 - Tiamină"}, {0, "mg", "E Alfa-tocoferol"},
                    {0.12, "mg", "B2 - Riboflavină" }, {5.644, "mg", "B3 - Niacină"}, {0.694, "mg", "B5 - Acid pantotenic" }, {0, "mcg", "K - Filochinonă" },
                    {0.149, "mg", "B6"}, {0, "mg", "C"}, {98, "mcg", "B9 - Acid folic"}, {141, "mg", "Calciu" }, {1.488, "mg", "Cupru"},
                    {6.35, "mg", "Fier" }, {790, "mg", "Fosfor" }, {353, "mg", "Magneziu"}, {1.457, "mg", "Mangan"}, {459, "mg", "Potasiu"},
                    {0, "mcg", "Seleniu"}, {1, "mg", "Sodiu"}, {10.45, "mg", "Zinc"}, {0, "mg", "B4 - Colină"}};
                for (int i = 0; i < tahiniNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "măsline negre"),
                        Quantity = Convert.ToDouble(tahiniNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(tahiniNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(tahiniNutrients[i, 1]))
                    });
                }

                object[,] sunflowerSeedsNutrients = new object[,] { { 4.73, "g", "Apă" }, { 584, "kcal", "Aport caloric" }, { 20, "g", "Carbohidrați"}, { 8.6, "g", "Fibre"}, {2.62, "g", "Glucide"},
                    {51.46, "g", "Lipide" }, {20.78, "g", "Proteine"}, {3, "mcg", "A - RAE" }, {1.48, "mg", "B1 - Tiamină"}, {35.17, "mg", "E Alfa-tocoferol"},
                    {0.355, "mg", "B2 - Riboflavină" }, {8.355, "mg", "B3 - Niacină"}, {1.13, "mg", "B5 - Acid pantotenic" }, {0, "mcg", "K - Filochinonă" },
                    {1.345, "mg", "B6"}, {1.4, "mg", "C"}, {227, "mcg", "B9 - Acid folic"}, {78, "mg", "Calciu" }, {1.8, "mg", "Cupru"},
                    {5.25, "mg", "Fier" }, {660, "mg", "Fosfor" }, {325, "mg", "Magneziu"}, {1.95, "mg", "Mangan"}, {645, "mg", "Potasiu"},
                    {53, "mcg", "Seleniu"}, {9, "mg", "Sodiu"}, {5, "mg", "Zinc"}, {55.1, "mg", "B4 - Colină"}};
                for (int i = 0; i < sunflowerSeedsNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "seminţe de floarea soarelui"),
                        Quantity = Convert.ToDouble(sunflowerSeedsNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(sunflowerSeedsNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(sunflowerSeedsNutrients[i, 1]))
                    });
                }

                object[,] hummusNutrients = new object[,] { {64.9, "g", "Apă" }, { 177, "kcal", "Aport caloric" }, { 20.1, "g", "Carbohidrați"}, { 4, "g", "Fibre"}, {0.3, "g", "Glucide"},
                    {8.6, "g", "Lipide" }, {4.9, "g", "Proteine"}, {0, "mcg", "A - RAE" }, {0.1, "mg", "B1 - Tiamină"}, {0.7, "mg", "E Alfa-tocoferol"},
                    {0.1, "mg", "B2 - Riboflavină" }, {0.4, "mg", "B3 - Niacină"}, {0.3, "mg", "B5 - Acid pantotenic" }, {3, "mcg", "K - Filochinonă" },
                    {0.4, "mg", "B6"}, {7.9, "mg", "C"}, {59, "mcg", "B9 - Acid folic"}, {49.0, "mg", "Calciu" }, {0.2, "mg", "Cupru"},
                    {1.6, "mg", "Fier" }, {110, "mg", "Fosfor" }, {29, "mg", "Magneziu"}, {0.6, "mg", "Mangan"}, {173, "mg", "Potasiu"},
                    {2.4, "mcg", "Seleniu"}, {242, "mg", "Sodiu"}, {1.1, "mg", "Zinc"}, {27.8, "mg", "B4 - Colină"}};
                for (int i = 0; i < hummusNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "hummus"),
                        Quantity = Convert.ToDouble(hummusNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(hummusNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(hummusNutrients[i, 1]))
                    });
                }

                object[,] blackBeansNutrients = new object[,] { { 66.4, "g", "Apă" }, { 132, "kcal", "Aport caloric" }, { 23.71, "g", "Carbohidrați"}, { 8.7, "g", "Fibre"}, {0, "g", "Glucide"},
                    {0.54, "g", "Lipide" }, {8.86, "g", "Proteine"}, {0, "mcg", "A - RAE" }, {0.244, "mg", "B1 - Tiamină"}, {0, "mg", "E Alfa-tocoferol"},  {149, "mcg", "B9 - Acid folic"},
                    {0.059, "mg", "B2 - Riboflavină" }, {0.505, "mg", "B3 - Niacină"}, {0.242, "mg", "B5 - Acid pantotenic" }, {0, "mcg", "K - Filochinonă" },
                    {0.069, "mg", "B6"}, {27, "mg", "Calciu" }, {0.209, "mg", "Cupru"},
                    {2.1, "mg", "Fier" }, {140, "mg", "Fosfor" }, {70, "mg", "Magneziu"}, {0.444, "mg", "Mangan"}, {355, "mg", "Potasiu"},
                    {1.2, "mcg", "Seleniu"}, {237, "mg", "Sodiu"}, {1.12, "mg", "Zinc"} };
                for (int i = 0; i < pickledCabbageNutrients.GetLength(0); i++)
                {
                    nutrientIngredients.Add(new NutrientIngredient
                    {
                        Ingredient = contextIngredients.First(x => x.Name == "fasole neagră"),
                        Quantity = Convert.ToDouble(blackBeansNutrients[i, 0]),
                        Nutrient = contextNutrients.First(x => x.Name == Convert.ToString(blackBeansNutrients[i, 2])),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(blackBeansNutrients[i, 1]))
                    });
                }

                //object[,] garliclNutrients = new object[,] { };
                foreach (NutrientIngredient nutrientIngredient in nutrientIngredients)
                {
                    _context.NutrientIngredients.Add(nutrientIngredient);
                }
                _context.SaveChanges();
            }

            IList<RecipeCategory> recipeCategories = new List<RecipeCategory>();
            if (!_context.RecipeIngredients.Any())
            {
                IEnumerable<string> names = new List<string> { "Deserturi", "Supe", "Paste", "Garnituri", "Salate", "Sucuri", "Smoothie-uri", "Aperitive", "Feluri principale" };

                foreach (var name in names)
                {
                    recipeCategories.Add(new RecipeCategory { Name = name });
                }

                foreach (RecipeCategory recipeCategory in recipeCategories)
                {
                    _context.RecipeCategories.Add(recipeCategory);
                }
                _context.SaveChanges();
            }

            IList<Recipe> recipes = new List<Recipe>();
            if (!_context.Recipes.Any())
            {
                Func<RecipeCategory, bool> mainDishes = x => x.Name == "Feluri principale";
                Func<RecipeCategory, bool> soups = x => x.Name == "Supe";
                Func<RecipeCategory, bool> aperitives = x => x.Name == "Aperitive";
                Func<RecipeCategory, bool> sideDishes = x => x.Name == "Garnituri";
                Func<RecipeCategory, bool> salads = x => x.Name == "Salate";
                Func<RecipeCategory, bool> juices = x => x.Name == "Sucuri";
                Func<RecipeCategory, bool> smoothies = x => x.Name == "Smoothie-uri";
                Func<RecipeCategory, bool> pasta = x => x.Name == "Paste";
                Func<RecipeCategory, bool> deserts = x => x.Name == "Deserturi";


                var contextRecipeCategories = _context.RecipeCategories.ToList();
                recipes.Add(new Recipe
                {
                    Name = "Supă cremă de roșii",
                    Category = contextRecipeCategories.FirstOrDefault(soups),
                    Directions = "1. Toate legumele şi zarzavaturile se curăţă şi se spală, se taie în jumătăţi şi se pun în oala de supă. Aceasta ar fi bine să fie de aproximativ 4 l, deoarece zeama trebuie să scadă la jumătate în timpul fierberii. Se pun toate să fiarbă în acelaşi timp cu sarea şi uleiul, iar oala se acoperă cu un capac.2.În timp ce fierb legumele, se pregătesc roşiile, se opăresc, se decojesc şi se zdrobesc cu mixerul sau se dau prin răzătoare.3.Se verifică cu o furculiţă dacă s - au fiert toate legumele, iar dacă s - au fiert se scot din zeama care au fiert pentru a se zdrobi cu blenderul până capătă consistenţa unui piure foarte fin.Piureul de legume se amestecă cu zeama şi se formează supa cremă. 4.Se adugă şi sucul de roşii şi se pune la fiert să mai dea în câteva clocote. Se potriveşte cu sare după gust şi se serveşte cu crutoane de pâine şi pătrunjel.",
                    Photo = ReadImage("E:\\Licenta1\\API proj\\HappyBroccoli.Repos\\Images\\SupaCremaDeRosii.jpg")
                });
                recipes.Add(new Recipe
                {
                    Name = "Paste bolognese",
                    Category = contextRecipeCategories.FirstOrDefault(pasta),
                    Directions = "Fierbe spaghetele conform instructiunilor de pe ambalaj. Incinge uleiul intr - o tigaie adanca. 2. Adauga ceapa si usturoiul.Adauga texturatul de soia granule si amesteca. 3. Toarna o cană de apă și adauga restul ingredientelor. 4. Gateste sosul la foc mic timp de 15 minute, pana este absorbit tot lichidul. 5. Serveste - l peste pastele fierte.",
                    Photo = ReadImage("E:\\Licenta1\\API proj\\HappyBroccoli.Repos\\Images\\PasteBolognese.jpg")
                });
                recipes.Add(new Recipe
                {
                    Name = "Ghiveci de legume",
                    Category = contextRecipeCategories.FirstOrDefault(mainDishes),//Fel Princilap
                    Directions = "Mod de preparare...",
                    Photo = ReadImage("E:\\Licenta1\\API proj\\HappyBroccoli.Repos\\Images\\GhiveciLegume.jpg")
                });
                recipes.Add(new Recipe
                {
                    Name = "Falafel",
                    Category = contextRecipeCategories.FirstOrDefault(mainDishes),
                    Directions =
                        "1. Nautul se da prin robotul de bucatarie, impreuna cu apa, pana cand se obtine o pasta, nu foarte fina si care nu se desface (se poate da si prin masina de tocat de 2-3 ori). 2. Intr-un castron se amesteca toate ingredientele, cu mana, pana se incorporeaza complet.  Se da la frigider pentru cel putin o ora. 3. Cu mainile umede se formeaza mingiute de dimensiunea unei nuci, se preseaza bine in mana, apoi se pot turti putin. Se tavalesc prin semintele de susan. Se prajesc pe ambele parti, in uleiul incins, pana cand se rumenesc (3-4 minute pe fiecare parte). Incercati mai intai sa prajiti o bucatica, daca se desface atunci mai adaugati putina faina si repetati procesul. 4. Se servesc calde, eventual alturi de hummus, painici pita, sos tahini si salata asortata/muraturi.",
                    Photo = ReadImage("E:\\Licenta1\\API proj\\HappyBroccoli.Repos\\Images\\Falafel.jpg")
                });
                recipes.Add(new Recipe
                {
                    Name = "Ciorbă de perișoare",
                    Category = contextRecipeCategories.FirstOrDefault(soups),
                    Directions = "1. Pentru perisoare: 2. Granulele de soia se fierb conform indicatiilor de pe pachet apoi se dau prin masina de tocat. 2.Orezul se fierbe separat si se scurge de apa. 3. Se amesteca toate ingredientele pana cand se obtine o pasta omogena. 4. Se ia putina compozitie in mana si se strange, daca se desface se mai adauga putina faina de naut. Este posibil sa mai fie nevoie si de putina apa. 5. Se lasa sa stea 30-60 minute la rece. 6. Se formeaza perisoarele si se coc in cuptor, la foc mediu (175 C) aproximativ 20-25 minute. 7. Se pun in ciorba cu putin timp inainte de consum. 8. Pentru ciorba: 9. Se taie tot zarzavatul mărunt. 10. Într-o oala se pun la fiert în 2 litri de apă, ceapa și fasolea verde. 11. Se lasă să fiarbă 5 minute apoi se adaugă restul legumelor. 12. Se continuă fierberea pentru încă 5-6 minute apoi se adaugă roșiile decojite și tăiate foarte fin/conserva de roșii. 13. La sfarșit se adaugă borșul / zeama de lămaie (dacă se va folosi zeamă de lămaie atunci se va mai completa cu apă). Se lasă să dea cateva clocote. 14. Se adaugă leușteanul tăiat fin, uleiul de măsline, sarea si piperul după gust. ",
                    Photo = ReadImage("E:\\Licenta1\\API proj\\HappyBroccoli.Repos\\Images\\CiorbaPerisoare.jpg")

                });
                recipes.Add(new Recipe
                {
                    Name = "Sparanghel in stil chinezesc",
                    Category = contextRecipeCategories.FirstOrDefault(mainDishes),
                    Directions = "1. Se amestecă toate ingredientele pentru marinată(sos de soia, coriandru, ghimbir, usturoi, fulgi de ardei iute). Se toarna peste bucatile de tofu, se amesteca bine si se lasa la marinat in frigider cel putin o oră. 2. Intr-un wok se incinge uleiul de susan, se adauga ceapa si bucatile de tofu si se rumenesc 2-3 minute. 3. e adauga sparanghelul si ardeiul gras, se mai căleste pret de 3-4 minute, amestecandu-se continuu. 4. La sfarșit se adaugă ceapa verde si semintele de susan. Se amestecă totul, pe foc, timp de cateva minute. Daca este nevoie se mai adauga sos de soia. 5. Se servește alături de orez fiert.",
                    Photo = ReadImage("E:\\Licenta1\\API proj\\HappyBroccoli.Repos\\Images\\SparanghelChinezesc.jpg")

                });

                recipes.Add(new Recipe
                {
                    Name = "Budincă de chia",
                    Category = contextRecipeCategories.FirstOrDefault(deserts),
                    Directions = " 1. Într-un bol mixăm banane, lapte, curmale şi unt de arahide. Amestecul turnăm într-un alt bol, adăugăm seminţe de chia şi amestecăm bine. 2. Acoperim bolul şi îl introducem în frigider pentru minim 4 ore sau peste noapte. 3. Înainte de servire iarăsi amestecăm şi putem decora cu banane sau migdale.",
                    Photo = ReadImage("E:\\Licenta1\\API proj\\HappyBroccoli.Repos\\Images\\BudincaChia.jpg")

                });

                recipes.Add(new Recipe
                {
                    Name = "Spumă de ciocolată",
                    Category = contextRecipeCategories.FirstOrDefault(deserts),
                    Directions = "1.Tofu se scurge bine de lichid. Se pune in blender impreuna cu curmalele. Se mixeaza pana cand curmalele sunt complet integrate in compozitie. 2.Ciocolata se topeste in bain-marie (baie de abur). Se toarna peste tofu in timp ce blenderul este pornit. 3. Se toarna spuma de ciocolata in pahare si se orneaza dupa gust. Se poate servi direct sau pastra la frigider pana a doua zi. ",
                    Photo = ReadImage("E:\\Licenta1\\API proj\\HappyBroccoli.Repos\\Images\\SpumaCiocolata.jpg")

                });

                foreach (Recipe recipe in recipes)
                {
                    _context.Recipes.Add(recipe);
                }
                _context.SaveChanges();
            }

            IList<RecipeIngredient> recipeIngredients = new List<RecipeIngredient>();
            if (!_context.RecipeIngredients.Any())
            {
                var contextIngredients = _context.Ingredients.ToList();
                var contextMeasures = _context.Measures.ToList();
                var contextRecipe = _context.Recipes.ToList();
                object[,] supaCremaRosii = new object[,] { { 5, "bucăți", "roșii gătite" }, { 3, "bucăți", "cartofi" }, { 2, "bucăți", "ceapă gătită"}, { 1, "bucăți", "ardei gras verde gătit"},
                    { 1, "bucăți", "gogoșari gătiți" }, {1 , "bucăți", "păstârnac gătit"}, { 2, "lg", "ulei de măsline"}, {3 , "căței", "usturoi"}, { 0 , "", "sare"},
                    {0, "", "piper"} };
                for (int i = 0; i < supaCremaRosii.GetLength(0); i++)
                {
                    recipeIngredients.Add(new RecipeIngredient
                    {
                        Recipe = contextRecipe.First(x => x.Name == "Supă cremă de roșii"),
                        Ingredient = contextIngredients.FirstOrDefault(x => x.Name == Convert.ToString(supaCremaRosii[i, 2])),
                        Quantity = Convert.ToDouble(supaCremaRosii[i, 0]),
                        Measure = contextMeasures.FirstOrDefault(x => x.Name == Convert.ToString(supaCremaRosii[i, 1]))
                    });
                }
                object[,] pasteBolognese = new object[,] { { 250, "g", "paste" }, { 100, "g", "soia texturat" }, { 1, "bucăți", "ceapă gătită"},
                    {5 , "lg", "sos de tomate"}, { 2, "lgt", "oregano uscat"}, {4 , "căței", "usturoi"}, { 3 , "lg", "ulei de măsline"},
                };
                for (int i = 0; i < pasteBolognese.GetLength(0); i++)
                {
                    recipeIngredients.Add(new RecipeIngredient
                    {
                        Recipe = contextRecipe.First(x => x.Name == "Paste bolognese"),
                        Ingredient = contextIngredients.First(x => x.Name == Convert.ToString(pasteBolognese[i, 2])),
                        Quantity = Convert.ToDouble(pasteBolognese[i, 0]),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(pasteBolognese[i, 1]))
                    });
                }

                object[,] falafel = new Object[,] { { 500, "g", "năut" }, { 1, "bucăți", "ceapă gătită" }, { 5, "căței", "usturoi" },
                    { 1, "legătură", "pătrunjel" }, { 1, "legătură", "coriandru" }, { 3, "lg", "făină de grâu" }, { 1, "lgt", "bicarbonat de sodiu" },
                    { 1, "lgt", "chimion" }, { 1, "lgt", "boia dulce" }, { 3, "lg", "seminţe de susan" } };

                for (int i = 0; i < falafel.GetLength(0); i++)
                {
                    recipeIngredients.Add(new RecipeIngredient
                    {
                        Recipe = contextRecipe.First(x => x.Name == "Falafel"),
                        Ingredient = contextIngredients.First(x => x.Name == Convert.ToString(falafel[i, 2])),
                        Quantity = Convert.ToDouble(falafel[i, 0]),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(falafel[i, 1]))
                    });
                }

                object[,] veggieBallsSoup = new Object[,] { { 1, "cană", "soia texturat" }, { 50 ,"g", "orez" }, { 2, "lg", "boia dulce" },
                    { 1, "legătură", "pătrunjel" }, { 5, "lg", "făină de grâu" }, { 1, "bucăți", "ceapă gătită" }, };

                for (int i = 0; i < veggieBallsSoup.GetLength(0); i++)
                {
                    recipeIngredients.Add(new RecipeIngredient
                    {
                        Recipe = contextRecipe.First(x => x.Name == "Ciorbă de perișoare"),
                        Ingredient = contextIngredients.First(x => x.Name == Convert.ToString(veggieBallsSoup[i, 2])),
                        Quantity = Convert.ToDouble(veggieBallsSoup[i, 0]),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(veggieBallsSoup[i, 1]))
                    });
                }

                object[,] sparanghelChinezesc = new Object[,] { { 200, "g", "tofu" }, { 200, "g", "sparanghel" }, { 1, "bucăți", "ceapă gătită" },
                    { 1, "bucăți", "ardei gras roșu gătit" }, { 1, "legătură", "pătrunjel" }, { 2, "lg", "seminţe de susan" }, {10, "g", "ghimbir"},
                    {4, "lg", "sos de soia"}, {4, "căței", "usturoi"}, { 2, "lgt", "coriandru"}, {1, "lg", "ulei de susan"} };

                for (int i = 0; i < sparanghelChinezesc.GetLength(0); i++)
                {
                    recipeIngredients.Add(new RecipeIngredient
                    {
                        Recipe = contextRecipe.First(x => x.Name == "Sparanghel in stil chinezesc"),
                        Ingredient = contextIngredients.First(x => x.Name == Convert.ToString(sparanghelChinezesc[i, 2])),
                        Quantity = Convert.ToDouble(sparanghelChinezesc[i, 0]),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(sparanghelChinezesc[i, 1]))
                    });
                }

                object[,] ghiveciLegume = new Object[,] { { 5, "lg", "ulei de măsline" }, { 200, "g", "conopidă gătită" }, { 2, "bucăți", "ceapă gătită" },
                    { 2, "bucăți", "ardei gras roșu gătit" }, { 1, "legătură", "pătrunjel" }, { 1, "bucăți", "vinete gătite" }, {1, "bucăți", "morcovi gătiți"},
                    {6, "bucăți", "roșii gătite"}, {3, "bucăți", "cartofi"},
                    {4, "căței", "usturoi"}, { 1, "lgt", "cimbru"},  };

                for (int i = 0; i < ghiveciLegume.GetLength(0); i++)
                {
                    recipeIngredients.Add(new RecipeIngredient
                    {
                        Recipe = contextRecipe.First(x => x.Name == "Ghiveci de legume"),
                        Ingredient = contextIngredients.First(x => x.Name == Convert.ToString(ghiveciLegume[i, 2])),
                        Quantity = Convert.ToDouble(ghiveciLegume[i, 0]),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(ghiveciLegume[i, 1]))
                    });
                }

                object[,] budincaChia = new Object[,] { { 4, "lg", "seminţe de chia" }, { 1, "pahar", "lapte de soia" }, {4, "lg", "unt de arahide"}, { 2, "bucăți", "banane" },
                    {40, "g", "curmale"} };

                for (int i = 0; i < budincaChia.GetLength(0); i++)
                {
                    recipeIngredients.Add(new RecipeIngredient
                    {
                        Recipe = contextRecipe.First(x => x.Name == "Budincă de chia"),
                        Ingredient = contextIngredients.First(x => x.Name == Convert.ToString(budincaChia[i, 2])),
                        Quantity = Convert.ToDouble(budincaChia[i, 0]),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(budincaChia[i, 1]))
                    });
                }

                object[,] spumaCiocolata = new Object[,] { { 350, "g", "silken tofu" }, { 30, "g", "curmale" }, { 100, "g", "ciocolată amăruie > 70-85% cacao" } };

                for (int i = 0; i < spumaCiocolata.GetLength(0); i++)
                {
                    recipeIngredients.Add(new RecipeIngredient
                    {
                        Recipe = contextRecipe.First(x => x.Name == "Spumă de ciocolată"),
                        Ingredient = contextIngredients.First(x => x.Name == Convert.ToString(spumaCiocolata[i, 2])),
                        Quantity = Convert.ToDouble(spumaCiocolata[i, 0]),
                        Measure = contextMeasures.First(x => x.Name == Convert.ToString(spumaCiocolata[i, 1]))
                    });
                }

                foreach (RecipeIngredient recipeIngredient in recipeIngredients)
                {
                    _context.RecipeIngredients.Add(recipeIngredient);
                }
                _context.SaveChanges();
            }

            IList<ItemWeight> itemWeights = new List<ItemWeight>();
            if (!_context.ItemWeights.Any())
            {
                object[,] values = new object[,] { { "sos de tomate", "lg", 8 }, { "roșii gătite", "bucăți", 120 }, { "roșii", "bucăți", 123 }, { "ceapă gătită", "bucăți", 110 },
                    { "cartofi", "bucăți", 173 }, { "păstârnac gătit", "bucăți", 160 }, { "ardei gras verde gătit", "bucăți", 120 },
                    { "gogoșari gătiți", "bucăți", 122 }, { "pătrunjel", "legătură", 13 }, { "coriandru", "legătură", 13 }, { "făină de grâu", "lg", 14 }, { "seminţe de susan", "lg", 10 }, {"soia texturat", "cană", 90 },
                    {"orez", "cană", 140}, {"sos de soia", "lg", 18}, {"ulei de măsline", "lg", 14}, {"seminţe de chia", "lg", 12}, {"banane", "bucăți", 120}, {"unt de arahide", "lg", 18}, {"lapte de soia", "pahar", 200},
                    { "usturoi", "căței", 3}, { "ulei de susan", "lg", 0 }, { "coriandru", "lgt", 0}, {"cimbru", "lgt",  0}, {"oregano uscat", "lgt", 0 }, { "vinete gătite", "bucăți", 200 },
                    { "sfeclă gătită", "bucăți", 50}, {"varză de bruxelles gătită", "bucăți", 21}, {"morcovi gătiți", "bucăți", 61}, {"morcovi", "bucăți", 61}, {"zeamă de lămâie", "lgt", 9},
                    { "avocado", "bucăți", 170}, {"tahini", "lg", 14}, {"hummus", "lg", 15}, { "castraveți", "bucăți", 301}
                };
                var contextIngredients = _context.Ingredients.ToList();
                for (int i = 0; i < values.GetLength(0); i++)
                {
                    itemWeights.Add(new ItemWeight
                    {
                        Ingredient = contextIngredients.First(x => x.Name == Convert.ToString(values[i, 0])),
                        Measure = values[i, 1].ToString(),
                        Grams = Convert.ToDouble(values[i, 2])
                    });
                }
                foreach (ItemWeight item in itemWeights)
                {
                    _context.ItemWeights.Add(item);
                }
                _context.SaveChanges();
            }

        }
    }
}

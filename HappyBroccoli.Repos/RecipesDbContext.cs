﻿using System;
using System.Configuration;
using HappyBroccoli.DomainEntities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Protocols;

namespace HappyBroccoli.Repos
{
    public class RecipesDbContext : DbContext
    {
        public RecipesDbContext(DbContextOptions<RecipesDbContext> options) : base (options)
        {
           
        }
        
        public DbSet<NutrientCategory> NutrientCategories { get; set; }
        public DbSet<Nutrient> Nutrients { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Measure> Measures { get; set; }
        public DbSet<NutrientIngredient> NutrientIngredients { get; set; }
        public DbSet<RecipeCategory> RecipeCategories { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<RecipeIngredient> RecipeIngredients { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<ItemWeight> ItemWeights { get; set; }
        public DbSet<UserFavorite> UserFavorites { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("veg");

            modelBuilder.Entity<NutrientCategory>().ToTable("NutrientCategories");
            modelBuilder.Entity<Nutrient>().ToTable("Nutrients");
            modelBuilder.Entity<Ingredient>().ToTable("Ingredients");
            modelBuilder.Entity<Measure>().ToTable("Measures");
            modelBuilder.Entity<NutrientIngredient>().ToTable("NutrientIngredients");
            modelBuilder.Entity<RecipeCategory>().ToTable("RecipeCategories");
            modelBuilder.Entity<Recipe>().ToTable("Recipes");
            modelBuilder.Entity<RecipeIngredient>().ToTable("RecipeIngredients");
            modelBuilder.Entity<Rating>().ToTable("Rating");
            modelBuilder.Entity<ItemWeight>().ToTable("ItemWeights");
            modelBuilder.Entity<UserFavorite>().ToTable("UserFavorites");
        }
  
    }
}

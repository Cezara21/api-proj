﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HappyBroccoli.Repos.Shared
{
    public interface IRepository<T> where T:class
    {
        IQueryable<T> GetAll();

        T GetbyId(int id);

        void Add(T item);

        bool Update(T item);

        bool Delete(int id);
    }
}

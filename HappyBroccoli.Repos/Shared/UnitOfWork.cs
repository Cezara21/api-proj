﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Repos.Shared
{
    public class UnitOfWork :IUnitOfWork
    {
        private readonly RecipesDbContext _context;
        
        public UnitOfWork(RecipesDbContext context)
        {
            _context = context;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}

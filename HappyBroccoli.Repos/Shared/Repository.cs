﻿using HappyBroccoli.DomainEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HappyBroccoli.Repos.Shared
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        protected readonly RecipesDbContext _context;
        public Repository(RecipesDbContext context)
        {
            _context = context;
        }
        public void Add(T item)
        {
            if(item==null)
            {
                throw new Exception("Null item");
            }
            _context.Set<T>().Add(item);        
        }

        public bool Delete(int id)
        {
            T item = _context.Set<T>().Find(id);
            _context.Set<T>().Remove(item);
            
            return true;
        }

        public IQueryable<T> GetAll()
        {
            return _context.Set<T>();
        }

        public T GetbyId(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public bool Update(T item)
        {
            var entity =_context.Set<T>().Single(t => t.Id == item.Id);
            _context.Update(entity);
            
            return true;         
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Repos.Shared
{
    public interface IUnitOfWork
    {
        void Save();
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HappyBroccoli.Repos.Migrations
{
    public partial class modifyQuantity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Cantity",
                schema: "veg",
                table: "RecipeIngredients",
                newName: "Quantity");

            migrationBuilder.RenameColumn(
                name: "Cantitate",
                schema: "veg",
                table: "NutrientIngredients",
                newName: "Quantity");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Quantity",
                schema: "veg",
                table: "RecipeIngredients",
                newName: "Cantity");

            migrationBuilder.RenameColumn(
                name: "Quantity",
                schema: "veg",
                table: "NutrientIngredients",
                newName: "Cantitate");
        }
    }
}

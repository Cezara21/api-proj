﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HappyBroccoli.Repos.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "veg");

            migrationBuilder.CreateTable(
                name: "Ingredients",
                schema: "veg",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ingredients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Measures",
                schema: "veg",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Measures", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NutrientCategories",
                schema: "veg",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NutrientCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Nutrients",
                schema: "veg",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CategoryId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Nutrients", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Nutrients_NutrientCategories_CategoryId",
                        column: x => x.CategoryId,
                        principalSchema: "veg",
                        principalTable: "NutrientCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "NutrientIngredients",
                schema: "veg",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Cantitate = table.Column<int>(nullable: false),
                    IngredientId = table.Column<int>(nullable: false),
                    MeasureId = table.Column<int>(nullable: false),
                    NutrientId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NutrientIngredients", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NutrientIngredients_Ingredients_IngredientId",
                        column: x => x.IngredientId,
                        principalSchema: "veg",
                        principalTable: "Ingredients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NutrientIngredients_Measures_MeasureId",
                        column: x => x.MeasureId,
                        principalSchema: "veg",
                        principalTable: "Measures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NutrientIngredients_Nutrients_NutrientId",
                        column: x => x.NutrientId,
                        principalSchema: "veg",
                        principalTable: "Nutrients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NutrientIngredients_IngredientId",
                schema: "veg",
                table: "NutrientIngredients",
                column: "IngredientId");

            migrationBuilder.CreateIndex(
                name: "IX_NutrientIngredients_MeasureId",
                schema: "veg",
                table: "NutrientIngredients",
                column: "MeasureId");

            migrationBuilder.CreateIndex(
                name: "IX_NutrientIngredients_NutrientId",
                schema: "veg",
                table: "NutrientIngredients",
                column: "NutrientId");

            migrationBuilder.CreateIndex(
                name: "IX_Nutrients_CategoryId",
                schema: "veg",
                table: "Nutrients",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NutrientIngredients",
                schema: "veg");

            migrationBuilder.DropTable(
                name: "Ingredients",
                schema: "veg");

            migrationBuilder.DropTable(
                name: "Measures",
                schema: "veg");

            migrationBuilder.DropTable(
                name: "Nutrients",
                schema: "veg");

            migrationBuilder.DropTable(
                name: "NutrientCategories",
                schema: "veg");
        }
    }
}

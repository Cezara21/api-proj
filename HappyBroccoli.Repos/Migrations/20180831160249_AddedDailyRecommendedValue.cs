﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HappyBroccoli.Repos.Migrations
{
    public partial class AddedDailyRecommendedValue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "DailyRecommendedValue",
                schema: "veg",
                table: "Nutrients",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DailyRecommendedValue",
                schema: "veg",
                table: "Nutrients");
        }
    }
}

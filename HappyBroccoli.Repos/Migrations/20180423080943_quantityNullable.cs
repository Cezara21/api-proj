﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HappyBroccoli.Repos.Migrations
{
    public partial class quantityNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RecipeIngredients_Measures_MeasureId",
                schema: "veg",
                table: "RecipeIngredients");

            migrationBuilder.AlterColumn<int>(
                name: "Quantity",
                schema: "veg",
                table: "RecipeIngredients",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "MeasureId",
                schema: "veg",
                table: "RecipeIngredients",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_RecipeIngredients_Measures_MeasureId",
                schema: "veg",
                table: "RecipeIngredients",
                column: "MeasureId",
                principalSchema: "veg",
                principalTable: "Measures",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RecipeIngredients_Measures_MeasureId",
                schema: "veg",
                table: "RecipeIngredients");

            migrationBuilder.AlterColumn<int>(
                name: "Quantity",
                schema: "veg",
                table: "RecipeIngredients",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MeasureId",
                schema: "veg",
                table: "RecipeIngredients",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_RecipeIngredients_Measures_MeasureId",
                schema: "veg",
                table: "RecipeIngredients",
                column: "MeasureId",
                principalSchema: "veg",
                principalTable: "Measures",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HappyBroccoli.Repos.Migrations
{
    public partial class measureNameNull : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "veg",
                table: "Measures",
                nullable: true,
                oldClrType: typeof(string));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "veg",
                table: "Measures",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}

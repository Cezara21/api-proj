﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HappyBroccoli.Repos.Migrations.UsersDb
{
    public partial class vegSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "veg");

            migrationBuilder.RenameTable(
                name: "AspNetUserTokens",
                newSchema: "veg");

            migrationBuilder.RenameTable(
                name: "AspNetUsers",
                newSchema: "veg");

            migrationBuilder.RenameTable(
                name: "AspNetUserRoles",
                newSchema: "veg");

            migrationBuilder.RenameTable(
                name: "AspNetUserLogins",
                newSchema: "veg");

            migrationBuilder.RenameTable(
                name: "AspNetUserClaims",
                newSchema: "veg");

            migrationBuilder.RenameTable(
                name: "AspNetRoles",
                newSchema: "veg");

            migrationBuilder.RenameTable(
                name: "AspNetRoleClaims",
                newSchema: "veg");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "AspNetUserTokens",
                schema: "veg");

            migrationBuilder.RenameTable(
                name: "AspNetUsers",
                schema: "veg");

            migrationBuilder.RenameTable(
                name: "AspNetUserRoles",
                schema: "veg");

            migrationBuilder.RenameTable(
                name: "AspNetUserLogins",
                schema: "veg");

            migrationBuilder.RenameTable(
                name: "AspNetUserClaims",
                schema: "veg");

            migrationBuilder.RenameTable(
                name: "AspNetRoles",
                schema: "veg");

            migrationBuilder.RenameTable(
                name: "AspNetRoleClaims",
                schema: "veg");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HappyBroccoli.Repos.Migrations
{
    public partial class AddedItemWeight : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Quantity",
                schema: "veg",
                table: "RecipeIngredients",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "Quantity",
                schema: "veg",
                table: "NutrientIngredients",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.CreateTable(
                name: "ItemWeights",
                schema: "veg",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemWeights", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ItemWeights",
                schema: "veg");

            migrationBuilder.AlterColumn<int>(
                name: "Quantity",
                schema: "veg",
                table: "RecipeIngredients",
                nullable: true,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Quantity",
                schema: "veg",
                table: "NutrientIngredients",
                nullable: false,
                oldClrType: typeof(double));
        }
    }
}

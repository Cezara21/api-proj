﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Interfaces;
using HappyBroccoli.Repos.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Repos.Repositories
{
    public class IngredientRepository: Repository<Ingredient>, IIngredientRepository
    {
        public IngredientRepository(RecipesDbContext context): base(context)
        {

        }
    }
}

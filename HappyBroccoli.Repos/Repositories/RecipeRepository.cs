﻿using HappyBroccoli.Repos.Interfaces;
using HappyBroccoli.Repos.Shared;
using HappyBroccoli.DomainEntities.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace HappyBroccoli.Repos.Repositories
{
    public class RecipeRepository: Repository<Recipe>, IRecipeRepository
    {
        public RecipeRepository(RecipesDbContext context)
            : base(context) {  }

        public IEnumerable<Recipe> GetAllIncludingReferences()
        {
            var result = _context.Recipes.Include(x => x.Category);
            return result;
        }
    }
}

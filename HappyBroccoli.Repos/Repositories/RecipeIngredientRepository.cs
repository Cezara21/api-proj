﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Interfaces;
using HappyBroccoli.Repos.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HappyBroccoli.Repos.Repositories
{
    public class RecipeIngredientRepository: Repository<RecipeIngredient>, IRecipeIngredientRepository
    {
        public RecipeIngredientRepository(RecipesDbContext context) 
            :base(context) { }
       
        public IEnumerable<RecipeIngredient> GetRecipeIngredients(int recipeId)
        {
            var result = _context.RecipeIngredients.Include(x => x.Recipe).Include(x => x.Ingredient).Include(x => x.Measure).Where(x => x.Recipe.Id == recipeId).ToList();
            return result;
        }

        public IEnumerable<RecipeIngredient> GetAllIncludingReferences()
        {
            var result = _context.RecipeIngredients.Include(x => x.Recipe).Include(x => x.Ingredient).Include(x => x.Measure).ToList();
            return result;
        }
    }
}

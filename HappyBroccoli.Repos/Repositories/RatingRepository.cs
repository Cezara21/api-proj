﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Interfaces;
using HappyBroccoli.Repos.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Repos.Repositories
{
    public class RatingRepository : Repository<Rating>, IRatingRepository
    {
        public RatingRepository(RecipesDbContext context)
            : base(context) { }

        public IEnumerable<Rating> GetAllIncludingReferences()
        {
            var result = _context.Ratings.Include(x => x.Recipe);

            return result;
        }
    }
}

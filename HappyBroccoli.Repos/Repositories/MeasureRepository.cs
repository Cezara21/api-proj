﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Interfaces;
using HappyBroccoli.Repos.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Repos.Repositories
{
    public class MeasureRepository: Repository<Measure>, IMeasureRepository
    {
        public MeasureRepository(RecipesDbContext context): base(context)
        {

        }
    }
}

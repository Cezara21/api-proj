﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Interfaces;
using HappyBroccoli.Repos.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Repos.Repositories
{
    public class ItemWeightRepository : Repository<ItemWeight>, IItemWeightRepository
    {
        public ItemWeightRepository(RecipesDbContext context) 
            : base(context) { }

        public IEnumerable<ItemWeight> GetAllIncludingReferences()
        {
            var result = _context.ItemWeights.Include(x => x.Ingredient);
            return result;
        }
    }
}

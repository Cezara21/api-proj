﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Interfaces;
using HappyBroccoli.Repos.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Repos.Repositories
{
    public class NutrientIngredientRepository : Repository<NutrientIngredient>, INutrientIngredientRepository
    {
        public NutrientIngredientRepository(RecipesDbContext context) : base(context) { }

        public IEnumerable<NutrientIngredient> GetAllIncludingReferences()
        {
            var result = _context.NutrientIngredients.Include(x => x.Ingredient).Include(x => x.Measure).Include(x => x.Nutrient).Include(x=>x.Nutrient.Category);
            return result;
        }
    }
}

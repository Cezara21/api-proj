﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Interfaces;
using HappyBroccoli.Repos.Shared;

namespace HappyBroccoli.Repos.Repositories
{
    public class RecipeCategoryRepository : Repository<RecipeCategory>, IRecipeCategoryRepository
    {
        public RecipeCategoryRepository(RecipesDbContext context)
            : base(context) { }

    }

}

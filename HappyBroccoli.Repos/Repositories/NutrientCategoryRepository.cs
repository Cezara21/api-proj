﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Shared;
using HappyBroccoli.Repos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Repos.Repositories
{
    public class NutrientCategoryRepository: Repository<NutrientCategory>, INutrientCategoryRepository
    {
        public NutrientCategoryRepository(RecipesDbContext context)
            : base(context) { }
    }
}

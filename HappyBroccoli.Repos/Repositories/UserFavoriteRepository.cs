﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Interfaces;
using HappyBroccoli.Repos.Shared;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace HappyBroccoli.Repos.Repositories
{
    public class UserFavoriteRepository : Repository<UserFavorite>, IUserFavoriteRepository
    {
        public UserFavoriteRepository(RecipesDbContext context) : base(context) { }


        public IEnumerable<UserFavorite> GetAllIncludingReferences()
        {
            var result = _context.UserFavorites.Include(x => x.Recipe);

            return result;
        }
    }

}

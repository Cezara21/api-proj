﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Shared;
using System.Collections.Generic;

namespace HappyBroccoli.Repos.Interfaces
{
    public interface IRecipeIngredientRepository: IRepository<RecipeIngredient>
    {
        IEnumerable<RecipeIngredient> GetRecipeIngredients(int recipeId);

        IEnumerable<RecipeIngredient> GetAllIncludingReferences();
    }
}

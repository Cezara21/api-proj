﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Shared;

namespace HappyBroccoli.Repos.Interfaces
{
    public interface IRecipeCategoryRepository : IRepository<RecipeCategory>
    {
        
    }
}

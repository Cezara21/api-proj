﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Shared;

namespace HappyBroccoli.Repos.Interfaces
{
    public interface INutrientRepository : IRepository<Nutrient>
    {

    }
}

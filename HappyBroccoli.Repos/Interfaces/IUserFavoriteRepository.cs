﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Shared;
using System.Collections.Generic;

namespace HappyBroccoli.Repos.Interfaces
{
    public interface IUserFavoriteRepository : IRepository<UserFavorite>
    {
        IEnumerable<UserFavorite> GetAllIncludingReferences();
    }
}

﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Repos.Interfaces
{
    public interface IRecipeRepository: IRepository<Recipe>
    {
        IEnumerable<Recipe> GetAllIncludingReferences();


    }
}

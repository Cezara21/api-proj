﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Shared;
using System.Collections.Generic;

namespace HappyBroccoli.Repos.Interfaces
{
    public interface IRatingRepository : IRepository<Rating>
    {
        IEnumerable<Rating> GetAllIncludingReferences();
    }
}

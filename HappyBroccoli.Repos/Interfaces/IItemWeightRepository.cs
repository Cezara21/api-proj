﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Repos.Interfaces
{
    public interface IItemWeightRepository : IRepository<ItemWeight>
    {
        IEnumerable<ItemWeight> GetAllIncludingReferences();
    }
}

﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Repos.Interfaces;
using HappyBroccoli.Repos.Shared;
using HappyBroccoli.Services.Common;
using HappyBroccoli.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HappyBroccoli.Services.Recipes
{
    public class RecipeService: IRecipeService
    {
        private readonly IRecipeRepository _recipeRepository;
        private readonly IRecipeIngredientRepository _recipeIngredientRepository;
        private readonly IIngredientRepository _ingredientRepository;
        private readonly INutrientIngredientRepository _nutrientIngredientRepository;
        private readonly IItemWeightRepository _itemWeightRepository;
        private readonly IRecipeCategoryRepository _recipeCategoryRepository;
        private readonly IMeasureRepository _measureRepository;
        private readonly IRatingRepository _ratingRepository;
        private readonly IUserFavoriteRepository _userFavoriteRepository;
        private readonly INutrientRepository _nutrientRepository;
        private readonly IUnitOfWork _unitOfWork;

        public RecipeService(IRecipeRepository repository,
            IRecipeIngredientRepository recipeIngredientRepository, 
            IIngredientRepository ingredientRepository, 
            INutrientIngredientRepository nutrientIngredientRepository,
            IItemWeightRepository itemWeightRepository,
            IRecipeCategoryRepository recipeCategoryRepository,
            IMeasureRepository measureRepository,
            IUserFavoriteRepository userFavoriteRepository,
            IRatingRepository ratingRepository,
            INutrientRepository nutrientRepository,
            IUnitOfWork unitOfWork)
        {
            _recipeRepository = repository;
            _unitOfWork = unitOfWork;
            _recipeIngredientRepository = recipeIngredientRepository;
            _ingredientRepository = ingredientRepository;
            _nutrientIngredientRepository = nutrientIngredientRepository;
            _itemWeightRepository = itemWeightRepository;
            _recipeCategoryRepository = recipeCategoryRepository;
            _measureRepository = measureRepository;
            _ratingRepository = ratingRepository;
            _userFavoriteRepository = userFavoriteRepository;
            _nutrientRepository = nutrientRepository;
        }

        public IEnumerable<string> GetCategories()
        {
            return _recipeCategoryRepository.GetAll().Select(x => x.Name).ToList();
        }

        public IEnumerable<string> GetRecipesNames()
        {
            return _recipeRepository.GetAll().Select(recipe => recipe.Name);
        }

        public IEnumerable<RecipeProfile> GetRecipesProfilesByIngredients(List<string> ingredients)
        {
            var returnedRecipes = new List<RecipeProfile>();
            var allRecipesList = _recipeRepository.GetAllIncludingReferences().ToList();
            var recipesRatingDict = new Dictionary<int, float>();
            foreach(var recipe in allRecipesList)
            {
                var rate = ComputeRate(recipe.Id);
                recipesRatingDict.Add(recipe.Id, rate);
            }

            if(ingredients == null)
            {
                returnedRecipes = allRecipesList.Select(recipe => MapRecipe(recipe, recipesRatingDict[recipe.Id])).ToList();
            }
            else
            {
                foreach (var recipe in allRecipesList)
                {
                    var recipeIngredients = _recipeIngredientRepository.GetAllIncludingReferences()
                        .Where(x => x.Recipe.Id == recipe.Id).Select(x => CommonMethods.RemoveLastWord(x.Ingredient.Name)).ToList();
                    if (ContainsAll(recipeIngredients, ingredients))
                    {
                        returnedRecipes.Add(MapRecipe(recipe, recipesRatingDict[recipe.Id]));
                    }
                }
               
            }
            return returnedRecipes;
        }

        private RecipeProfile MapRecipe(Recipe recipe, float ratingValue)
        {
            return new RecipeProfile
            {
                Id = recipe.Id,
                Name = recipe.Name,
                Category = recipe.Category.Name,
                Photo = Convert.ToBase64String(recipe.Photo),
                UserId = recipe.UserId,
                RatingValue = ratingValue
            };
        }

        private float ComputeRate(int id)
        {
            float value = 0;

            var values = _ratingRepository.GetAllIncludingReferences()
                .Where(x => x.Recipe.Id == id).Select(x => x.Value).ToList();
            if (!values.Any())
            {
                return 0;
            }
            value = values.Sum() / values.Count();

            return value;
        }

        public static bool ContainsAll(List<string> source, List<string> values)
        {
            return values.All(value => source.Contains(value));
        }

       
        public RecipeDetailsViewModel GetRecipeDetails(int recipeId)
        {
            var recipe = _recipeRepository.GetbyId(recipeId);

            return new RecipeDetailsViewModel
            {
                Id = recipe.Id,
                Name = recipe.Name,
                Directions = recipe.Directions,
                Photo = Convert.ToBase64String(recipe.Photo),
                RatingValue = ComputeRate(recipe.Id)
            };
        }

        public IEnumerable<RecipeNutrientsValueViewModel> GetNutritionalValuesForRecipe(int recipeId)
        {
            var recipeNutrientsValues = new List<RecipeNutrientsValueViewModel>();
            var recipe = _recipeRepository.GetbyId(recipeId);
            var customIngredientsForRecipe = _recipeIngredientRepository.GetRecipeIngredients(recipeId);
            bool nutrientExistsInViewModel = false;
            foreach(var customIngredient in customIngredientsForRecipe)
            {
                var customNutrientsForIngredient = _nutrientIngredientRepository
                    .GetAllIncludingReferences()
                    .Where(x => x.Ingredient == customIngredient.Ingredient);
                foreach(var customNutrientForIngredient in customNutrientsForIngredient)
                {
                    nutrientExistsInViewModel = recipeNutrientsValues.Exists(x => x.Name == customNutrientForIngredient.Nutrient.Name);
                    if (!nutrientExistsInViewModel)
                    {
                        AddNutrient(recipeNutrientsValues, customNutrientForIngredient);
                    }
                    var nutrientForCalculus = recipeNutrientsValues.Single(x => x.Name == customNutrientForIngredient.Nutrient.Name);
                    nutrientForCalculus.Value += CalculateNutrientValueForCustomIngredient(customIngredient, customNutrientForIngredient);
                }
            }

            foreach(var nutrientValue in recipeNutrientsValues)
            {
                var recommendedDailyValue = _nutrientRepository.GetAll()
                    .SingleOrDefault(x => x.Name == nutrientValue.Name).DailyRecommendedValue;
                double percent = recommendedDailyValue == 0 ? 0 : nutrientValue.Value * 100 / recommendedDailyValue;
                nutrientValue.PercentFromDRI = percent;

                ChangeUnitWhenAccurate(nutrientValue);
            }
            return recipeNutrientsValues.OrderBy(x=>x.Name);
        }

        private static void ChangeUnitWhenAccurate(RecipeNutrientsValueViewModel nutrientValue)
        {
            if (nutrientValue.Value >= 1000)
            {
                switch (nutrientValue.Measure)
                {
                    case "mg":
                        nutrientValue.Measure = "g";
                        nutrientValue.Value /= 1000;
                        break;
                    case "mcg":
                        nutrientValue.Measure = "mg";
                        nutrientValue.Value /= 1000;
                        break;
                    default:
                        break;
                }
            }
        }

        private static void AddNutrient(List<RecipeNutrientsValueViewModel> recipeNutrientsValues, NutrientIngredient customNutrientForIngredient)
        {
            recipeNutrientsValues.Add(new RecipeNutrientsValueViewModel
            {
                Name = customNutrientForIngredient.Nutrient.Name,
                Measure = customNutrientForIngredient.Measure.Name,
                Category = customNutrientForIngredient.Nutrient.Category.Name
            });
        }

        private double CalculateNutrientValueForCustomIngredient(RecipeIngredient customIngredient, NutrientIngredient customNutrientForIngredient)
        {
            double value;
            double nutrientQuantity = customNutrientForIngredient.Quantity ?? 0;
            double ingredientQuantity;
            if(customIngredient.Measure.Name == "g")
            {
                ingredientQuantity = customIngredient.Quantity ?? 0;
            }
            else
            {
                //valoarea in grame pentru bucati, lg etc
                ingredientQuantity = _itemWeightRepository.GetAll()
                    .Single(x => x.Ingredient == customIngredient.Ingredient && x.Measure == customIngredient.Measure.Name)
                    .Grams;
            }
            value = (ingredientQuantity * nutrientQuantity) / 100;
            return value;
        }

        public int AddRecipe(RecipeInfoRequestModel model)
        {
            var category = _recipeCategoryRepository.GetAll().Single(x => x.Name == model.Category);
            _recipeRepository.Add(new Recipe
            {
                Name = model.Title,
                Category = category,
                Directions = model.Directions,
                Photo = model.Photo,
                UserId = model.UserId
            });
            _unitOfWork.Save();
            var recipe = _recipeRepository.GetAll().Single(x => x.Name == model.Title);
            foreach(var customIngredient in model.CustomIngredients)
            {
                var ingredient = _ingredientRepository.GetAll().Single(x => x.Name == customIngredient.Ingredient);
                var measure = _measureRepository.GetAll().Single(x => x.Name == customIngredient.Measure);
                _recipeIngredientRepository.Add(new RecipeIngredient
                {
                    Ingredient = ingredient,
                    Recipe = recipe,
                    Measure = measure,
                    Quantity = customIngredient.Quantity
                });
            }
            _unitOfWork.Save();
            return recipe.Id;
        }

        public int AddUserFavorite(UserFavoriteRequestModel model)
        {
            var recipe = this._recipeRepository.GetbyId(model.RecipeId);

            _userFavoriteRepository.Add(new UserFavorite
            {
                Recipe = recipe,
                UserId = model.UserId
            });
            _unitOfWork.Save();

            return recipe.Id;
        }

        public IEnumerable<RecipeProfile> GetUserFavorites(string userId)
        {
            var recipeIds = this._userFavoriteRepository.GetAllIncludingReferences().Where(x => x.UserId == userId).Select(x => x.Recipe.Id).ToList();

            var favorites = this._recipeRepository.GetAll().Where(x => recipeIds.Contains(x.Id)).Select(recipe => new RecipeProfile
            {
                Id = recipe.Id,
                Name = recipe.Name,
                Photo = Convert.ToBase64String(recipe.Photo),
                Category = recipe.Category.Name
            }).ToList();

            return favorites;
        }

        public int RemoveUserFavorite(int recipeId, string userId)
        {
            var userFavoriteId = this._userFavoriteRepository.GetAll()
                .FirstOrDefault(x => x.Recipe.Id == recipeId && x.UserId == userId).Id;

            _userFavoriteRepository.Delete(userFavoriteId);
            _unitOfWork.Save();

            return recipeId;
        }

        public int AddRating(AddRatingRequestModel model)
        {
            var recipe = this._recipeRepository.GetbyId(model.RecipeId);

            var rating = this._ratingRepository.GetAllIncludingReferences().SingleOrDefault(x => x.UserId == model.UserId && x.Recipe.Id == model.RecipeId);
            if(rating!=null)
            {
                this._ratingRepository.Delete(rating.Id);
                _unitOfWork.Save();
            }
            _ratingRepository.Add(new Rating
            {
                Recipe = recipe,
                UserId = model.UserId,
                Value = model.Value
            });
            _unitOfWork.Save();

            return recipe.Id;
        }

        public float GetUserRating(string userId, int recipeId)
        {
            var result = this._ratingRepository.GetAllIncludingReferences()
                         .SingleOrDefault(x => x.Recipe.Id == recipeId && x.UserId == userId);

            return result == null ? 0 : result.Value;
        }
    }
}

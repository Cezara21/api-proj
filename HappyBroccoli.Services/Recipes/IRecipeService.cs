﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Services.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Services.Recipes
{
    public interface IRecipeService
    {
        IEnumerable<RecipeProfile> GetRecipesProfilesByIngredients(List<string> ingredients);

        RecipeDetailsViewModel GetRecipeDetails(int recipeId);

        IEnumerable<RecipeNutrientsValueViewModel> GetNutritionalValuesForRecipe(int recipeId);

        IEnumerable<string> GetCategories();

        int AddRecipe(RecipeInfoRequestModel model);

        IEnumerable<string> GetRecipesNames();

        int AddUserFavorite(UserFavoriteRequestModel model);

        IEnumerable<RecipeProfile> GetUserFavorites(string userId);

        int RemoveUserFavorite(int recipeId, string userId);

        int AddRating(AddRatingRequestModel model);

        float GetUserRating(string userId, int recipeId);
    }
}

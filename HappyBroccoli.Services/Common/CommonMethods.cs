﻿using System.Collections.Generic;
using System.Linq;

namespace HappyBroccoli.Services.Common
{
    public class CommonMethods
    {
        public static string RemoveLastWord(string name)
        {
            var unwantedWords = new List<string> { "gătită", "gătit", "gătite", "gătiți" };
            var words = name.Split(" ").ToList();
            if (unwantedWords.Contains(words[words.Count - 1]))
            {
                words.Remove(words[words.Count - 1]);
            }
            return string.Join(" ", words.ToArray());
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Services.Models
{
    public class AddRatingRequestModel
    {
        public string UserId { get; set; }

        public int RecipeId { get; set; }

        public float Value { get; set; }
    }
}

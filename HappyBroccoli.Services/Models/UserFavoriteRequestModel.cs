﻿namespace HappyBroccoli.Services.Models
{
    public class UserFavoriteRequestModel
    {
        public int RecipeId { get; set; }

        public string UserId { get; set; }
    }
}

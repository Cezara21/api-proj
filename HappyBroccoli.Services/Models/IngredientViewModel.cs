﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Services.Models
{
    public class IngredientViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}

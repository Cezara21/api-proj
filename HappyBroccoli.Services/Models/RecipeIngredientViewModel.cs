﻿using HappyBroccoli.DomainEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Services.Models
{
    public class RecipeIngredientViewModel
    {
        public int RecipeId { get; set; }

        public int IngredientID { get; set; }

        public string IngredientName { get; set; }

        public double? Quantity { get; set; }

        public string MeasureName { get; set; }
    }
}

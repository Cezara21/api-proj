﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Services.Models
{
    public class RecipeNutrientsValueViewModel
    {
        public string Name { get; set; }

        public double Value { get; set; }

        public string Measure { get; set; }

        public string Category { get; set; }

        public double PercentFromDRI { get; set; }
    }
}

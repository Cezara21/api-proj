﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Services.Models
{
    public class RecipeDetailsViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Directions { get; set; }

        public string Photo { get; set; }

        public float RatingValue { get; set; }

    }
}

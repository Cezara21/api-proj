﻿namespace HappyBroccoli.Services.Models
{
    public class LoggedInUser
    {
        public string Email { get; set; }

        public string Token { get; set; }

        public string Name { get; set; }

        public string Id { get; set; }
    }
}

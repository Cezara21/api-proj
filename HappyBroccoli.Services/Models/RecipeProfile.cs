﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Services.Models
{
    public class RecipeProfile
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Photo { get; set; }

        public string Category { get; set; }

        public string UserId { get; set; }

        public float RatingValue { get; set; }

    }
}

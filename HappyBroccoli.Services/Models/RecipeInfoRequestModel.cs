﻿using System.Collections.Generic;
using System;

namespace HappyBroccoli.Services.Models
{
    public class RecipeInfoRequestModel
    {
        public string Title { get; set; }

        public string Category { get; set; }

        public List<CustomIngredientModel> CustomIngredients { get; set; }

        public string Directions { get; set; }

        public byte[] Photo { get; set; } = new byte[] { };

        public string UserId { get; set; }

    }
}

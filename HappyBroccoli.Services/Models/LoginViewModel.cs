﻿using System.ComponentModel.DataAnnotations;

namespace HappyBroccoli.Services.Models
{
    public class LoginViewModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

    }
}

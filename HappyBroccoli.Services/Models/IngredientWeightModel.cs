﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Services.Models
{
    public class IngredientWeightModel
    {
        public string IngredientName { get; set; }

        public string MeasureName { get; set; }
    }
}

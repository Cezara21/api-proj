﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Services.Models
{
    public class CustomIngredientModel
    {
        public string Ingredient { get; set; }

        public string Measure { get; set; }
        
        public double Quantity { get; set; }
    }
}

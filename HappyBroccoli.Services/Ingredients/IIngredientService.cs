﻿using HappyBroccoli.DomainEntities.Entities;
using HappyBroccoli.Services.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyBroccoli.Services.Ingredients
{
    public interface IIngredientService
    {
        IEnumerable<string> GetAll();

        IEnumerable<string> GetFullNameIngredients();

        IEnumerable<RecipeIngredientViewModel> GetRecipeCustomIngredients(int recipeId);

        IEnumerable<IngredientViewModel> GetAutoCompleteIngredients(string value);

        IEnumerable<string> GetMeasures();

        IEnumerable<IngredientWeightModel> GetCustomIngredientMeasures();
    }
}

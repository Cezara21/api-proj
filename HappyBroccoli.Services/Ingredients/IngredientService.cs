﻿using System;
using System.Collections.Generic;
using HappyBroccoli.DomainEntities.Entities;
using System.Linq;
using HappyBroccoli.Repos.Interfaces;
using HappyBroccoli.Services.Models;
using HappyBroccoli.Repos.Shared;
using HappyBroccoli.Services.Common;

namespace HappyBroccoli.Services.Ingredients
{
    public class IngredientService : IIngredientService
    {
        private readonly IIngredientRepository _repository;
        private readonly IMeasureRepository _measureRepository;
        private readonly IItemWeightRepository _itemWeightRepository;
        private readonly IRecipeIngredientRepository _recipeIngredientRepository;

        private readonly IUnitOfWork _unitOfWork;
           
        public IngredientService(IIngredientRepository repository,
            IRecipeIngredientRepository recipeIngredientRepository,
            IMeasureRepository measureRepository,
            IItemWeightRepository itemWeightRepository,
            IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _measureRepository = measureRepository;
            _recipeIngredientRepository = recipeIngredientRepository;
            _itemWeightRepository = itemWeightRepository;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<string> GetAll()
        {
            return _repository.GetAll().Select(ingredient => CommonMethods.RemoveLastWord(ingredient.Name)).Distinct().ToList();
        }

        public IEnumerable<string> GetFullNameIngredients()
        {
            return _repository.GetAll().Select(ingredient => ingredient.Name);
        }

        public IEnumerable<IngredientViewModel> GetAutoCompleteIngredients(string value)
        {
            return _repository.GetAll()
                .Where(ingredient => ingredient.Name.Contains(value))
                .Select(ingredient => new IngredientViewModel
                { Id = ingredient.Id, Name = ingredient.Name }).ToList();
        }

        public IEnumerable<string> GetMeasures()
        {
            return _measureRepository.GetAll().Select(x => x.Name).ToList();
        }

        public IEnumerable<RecipeIngredientViewModel> GetRecipeCustomIngredients(int recipeId)
        {
            return _recipeIngredientRepository.GetRecipeIngredients(recipeId).Select(recipeIngredient => new RecipeIngredientViewModel
            {
                RecipeId = recipeIngredient.Recipe.Id,
                IngredientID = recipeIngredient.Ingredient.Id,
                Quantity = recipeIngredient.Quantity,
                MeasureName = recipeIngredient.Measure != null ? recipeIngredient.Measure.Name : "",
                IngredientName = CommonMethods.RemoveLastWord(recipeIngredient.Ingredient.Name)
            });
        }

        public IEnumerable<IngredientWeightModel> GetCustomIngredientMeasures()
        {
            return _itemWeightRepository.GetAllIncludingReferences().Select(x => new IngredientWeightModel
            {
                IngredientName = x.Ingredient.Name,
                MeasureName = x.Measure
            });
        }
    }
}

﻿using HappyBroccoli.API.Helpers;
using HappyBroccoli.API.Models;
using HappyBroccoli.DomainEntities;
using HappyBroccoli.Repos;
using HappyBroccoli.Services.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HappyBroccoli.API.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly UserManager<AppUser> _userManager;

        private readonly SignInManager<AppUser> _signInManager;

        private readonly JwtIssuerOptions _jwtIssuerOptions;

        private IJwtFactory _jwtFactory;

        public UserController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, IJwtFactory jwtFactory, IOptions<JwtIssuerOptions> jwtIssuerOptions)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtIssuerOptions = jwtIssuerOptions.Value;
            _jwtFactory = jwtFactory;
        }

        [HttpPost("[action]")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody]RegisterViewModel model)
        {
            var user = new AppUser { Email = model.Email, FirstName = model.FirstName, LastName = model.LastName, UserName = model.Email };
            var result = await _userManager.CreateAsync(user, model.Password);

            if(result.Succeeded)
            {
                return Ok(user);
            }

            return Unauthorized();
        }
        [HttpPost("[action]")]
        public async Task<IActionResult> Login([FromBody]LoginViewModel credentials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userToVerify = await _userManager.FindByEmailAsync(credentials.Email);

            var identity = await GetClaimsIdentity(userToVerify, credentials.Password);
            if (identity == null)
            {
                return BadRequest(Errors.AddErrorToModelState("login_failure", "Invalid username or password.", ModelState));
            }

            var jwt = await Tokens.GenerateJwt(identity, _jwtFactory, credentials.Email, _jwtIssuerOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });
            var name = userToVerify.FirstName + " " + userToVerify.LastName;
            var id = userToVerify.Id;
            var loggedInUser = new LoggedInUser()
            {
                Id = id,
                Email = credentials.Email,
                Token = jwt,
                Name = name
            };

            return new OkObjectResult(loggedInUser);
        }

        

        private async Task<ClaimsIdentity> GetClaimsIdentity(AppUser userToVerify, string password)
        {
            if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);
            if (string.IsNullOrEmpty(userToVerify.Email) || string.IsNullOrEmpty(password))
                return await Task.FromResult<ClaimsIdentity>(null);

            // get the user to verifty


            // check the credentials
            if (await _userManager.CheckPasswordAsync(userToVerify, password))
            {
                return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(userToVerify.Email, userToVerify.Id));
            }

            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<ClaimsIdentity>(null);
        }

    }
}

﻿using System.Collections.Generic;
using System.Linq;
using HappyBroccoli.Services.Ingredients;
using HappyBroccoli.Services.Models;
using Microsoft.AspNetCore.Mvc;

namespace HappyBroccoli.Controllers
{
    [Produces("application/json")]
    [Route("ingredients/")]
    public class IngredientController : Controller
    {
        private readonly IIngredientService _service;

        public IngredientController(IIngredientService service)
        {
            _service = service;
        }      

        [HttpGet]
        public IActionResult GetAllIngredients()
        {
            IEnumerable<string> list = _service.GetAll();

            return Ok(list);

        }

        [HttpGet]
        [Route("fullname")]
        public IActionResult GetFullNameIngredients()
        {
            return Ok(_service.GetFullNameIngredients());
        }

        [HttpGet]
        [Route("{recipeId}")]
        public IActionResult GetRecipeIngredients(int recipeId)
        {
            return Ok(_service.GetRecipeCustomIngredients(recipeId));
        }

        [HttpGet]
        [Route("measures")]
        public IActionResult GetMeasures()
        {
            IEnumerable<string> list = _service.GetMeasures();

            return Ok(list);
        }

        [HttpGet]
        [Route("autocomplete")]
        public IActionResult GetAutoCompleteIngredients(string value)
        {
            IEnumerable<IngredientViewModel> list = _service.GetAutoCompleteIngredients(value);

            if (list.Any())
            {
                return Ok(list);
            }

            return NoContent();
        }

        [HttpGet]
        [Route("measures/custom")]
        public IActionResult GetCustomIngredientMeasures()
        {
            return Ok(_service.GetCustomIngredientMeasures());
        }
    }
}
﻿using System.Collections.Generic;
using HappyBroccoli.Services.Models;
using HappyBroccoli.Services.Recipes;
using Microsoft.AspNetCore.Mvc;

namespace HappyBroccoli.API.Controllers
{
    [Produces("application/json")]
    [Route("api/")]
    public class RecipeController : Controller
    {
        IRecipeService _recipeService;

        public RecipeController(IRecipeService service)
        {
            _recipeService = service;
        }

        // GET: api/Default

        [HttpPost]
        [Route("recipes/ingredients")]
        public IActionResult GetRecipeProfilesFiltered([FromBody]List<string> ingredients)
        {
            return Ok(_recipeService.GetRecipesProfilesByIngredients(ingredients));
        }

        [HttpGet]
        [Route("recipes/{recipeId}/details")]
        public IActionResult GetRecipeDetails(int recipeId)
        {
            return Ok(_recipeService.GetRecipeDetails(recipeId));
           
        }


        [HttpGet]
        [Route("recipes/names")]
        public IActionResult GetRecipesNames()
        {
            return Ok(_recipeService.GetRecipesNames());
        }

        [HttpGet]
        [Route("recipes/{recipeId}/nutritionalValues")]
        public IActionResult GetRecipeNutritionalValues(int recipeId)
        {
            return Ok(_recipeService.GetNutritionalValuesForRecipe(recipeId));
        }

        [HttpGet]
        [Route("recipes/categories")]
        public IActionResult GetCategories()
        {
            IEnumerable<string> list = _recipeService.GetCategories();

            return Ok(list);
        }

        [HttpPost]
        [Route("recipes")]
        public IActionResult AddRecipe([FromBody]RecipeInfoRequestModel recipe)
        {
            return Ok(_recipeService.AddRecipe(recipe));
        }

        [HttpPost]
        [Route("recipes/favorites")]
        public IActionResult AddUserFavorite([FromBody]UserFavoriteRequestModel model)
        {
            return Ok(_recipeService.AddUserFavorite(model));
        }

        [HttpGet]
        [Route("recipes/{userId}/favorites")]
        public IActionResult GetUserFavorites(string userId)
        {
            return Ok(_recipeService.GetUserFavorites(userId));
        }

        [HttpDelete]
        [Route("recipes/{recipeId}/{userId}")]
        public IActionResult DeleteUserFavorite(int recipeId, string userId)
        {
            return Ok(_recipeService.RemoveUserFavorite(recipeId, userId));
        }

        [HttpPost]
        [Route("recipes/rating")]
        public IActionResult AddOrUpdateRating([FromBody]AddRatingRequestModel model)
        {
            return Ok(_recipeService.AddRating(model));
        }

        [HttpGet]
        [Route("recipes/{recipeId}/{userId}")]
        public IActionResult GetUserRating(int recipeId, string userId)
        {
            return Ok(_recipeService.GetUserRating(userId, recipeId));
        }
    }
}


﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HappyBroccoli.DomainEntities.Entities
{
    public abstract class Entity
    {
        [Required]
        [Key]
        public int Id { get; set; }
        
    }
}

﻿using Microsoft.AspNetCore.Identity;

namespace HappyBroccoli.DomainEntities
{
    public class AppUser: IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}

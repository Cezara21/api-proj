﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HappyBroccoli.DomainEntities.Entities
{
    public class Ingredient:Entity
    {   
        [Required]
        public string Name { get; set; }
    }
}

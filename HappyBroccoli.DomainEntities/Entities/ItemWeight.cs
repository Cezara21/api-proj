﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HappyBroccoli.DomainEntities.Entities
{
    public class ItemWeight : Entity
    {
        [Required]
        public Ingredient Ingredient { get; set; }
        [Required]
        public string Measure { get; set; }
        [Required]
        public double Grams { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HappyBroccoli.DomainEntities.Entities
{
    public class NutrientCategory : Entity
    {
        [Required]
        public string Name { get; set; }
    }
}

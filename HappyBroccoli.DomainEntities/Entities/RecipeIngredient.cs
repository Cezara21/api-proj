﻿using System.ComponentModel.DataAnnotations;

namespace HappyBroccoli.DomainEntities.Entities
{
    public class RecipeIngredient:Entity
    {
        [Required]
        public Recipe Recipe { get; set; }

        [Required]
        public Ingredient Ingredient { get; set; }

        public double? Quantity { get; set; }

        public Measure Measure { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HappyBroccoli.DomainEntities.Entities
{
    public class NutrientIngredient: Entity
    {
        [Required]
        public Nutrient Nutrient { get; set; }
        [Required]
        public Ingredient Ingredient { get; set; }
        [Required]
        public double? Quantity { get; set; }
        [Required]
        public Measure Measure { get; set; }
    }
}

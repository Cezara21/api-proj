﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HappyBroccoli.DomainEntities.Entities
{
    public class Recipe:Entity
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public RecipeCategory Category { get; set; }

        [Required]
        public string Directions { get; set; }

        [Column(TypeName="image")]
        public byte[] Photo { get; set; }

        public string UserId { get; set; }

    }
}

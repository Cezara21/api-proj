﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HappyBroccoli.DomainEntities.Entities
{
    public class Rating: Entity
    {
        [Required]
        public Recipe Recipe { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required]
        public float Value { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HappyBroccoli.DomainEntities.Entities
{
    public class RecipeCategory:Entity
    {
        [Required]
        public string Name { get; set; }
    }
}
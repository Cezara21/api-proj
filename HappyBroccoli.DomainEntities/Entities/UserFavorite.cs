﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HappyBroccoli.DomainEntities.Entities
{
    public class UserFavorite : Entity
    {
        [Required]
        public Recipe Recipe { get; set; }

        [Required]
        public string UserId { get; set; }
    }
}

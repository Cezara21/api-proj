﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HappyBroccoli.DomainEntities.Entities
{
    public class Measure:Entity
    {
        public string Name { get; set; }
    }
}
